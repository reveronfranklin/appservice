﻿using AppService.Core.CustomEntities;
using AppService.Core.Entities;
using AppService.Core.QueryFilters;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppService.Core.Interfaces
{
    public interface ICobAdjuntosCobranzaService
    {
        PagedList<CobAdjuntosCobranza> GetCobAdjuntosCobranza(AdjuntosCobranzaFilter filters);
    }
}
