﻿using AppService.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppService.Core.Interfaces
{
    public interface ICobAdjuntosCobranzaRepository
    {

        IEnumerable<CobAdjuntosCobranza> GetAll();
     
    }


}
