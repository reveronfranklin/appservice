﻿using AppService.Core.Entities;
using System;
using System.Threading.Tasks;

namespace AppService.Core.Interfaces
{
    public interface IUnitOfWork: IDisposable
    {
        ICobAdjuntosCobranzaRepository CobAdjuntosCobranzaRepository { get; }

        ICobGeneralCobranzaRepository GeneralCobranzaRepository { get; }

        ISegUsuarioRepository SegUsuarioRepository { get; }

        IMtrClienteRepository MtrClienteRepository { get; }

        ICobTipoTransaccionRepository CobTipoTransaccionRepository { get; }

        IOfdTipoDocumentoRepository OfdTipoDocumentoRepository { get; }

        IMtrOficinaRepository MtrOficinaRepository { get; }

        IMtrVendedorRepository MtrVendedorRepository { get; }

        IMtrBancosRepository MtrBancosRepository { get; }

        IMtrTipoMonedaRepository MtrTipoMonedaRepository { get; }

        void SaveChanges();

        Task SaveChangesAsync();



    }
}
