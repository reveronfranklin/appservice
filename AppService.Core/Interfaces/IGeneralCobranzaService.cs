﻿using AppService.Core.CustomEntities;
using AppService.Core.Entities;
using AppService.Core.QueryFilters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Core.Interfaces
{
    public interface IGeneralCobranzaService
    {


        Task<PagedList<CobGeneralCobranza>> GetGeneralCobranza(GeneralCobranzaQueryFilter filters);

        Task<CobGeneralCobranza> GetGeneralCobranza(long documento);

        Task<CobGeneralCobranza> InsertGeneralCobranza(CobGeneralCobranza generalCobranza);

        Task<CobGeneralCobranza> UpdateGeneralCobranza(CobGeneralCobranza generalCobranza);

        Task<bool> DeleteGeneralCobranza(long documento);

        Task<Metadata> ValidaInsertCobranza(CobGeneralCobranza generalCobranza);

        Task<Metadata> ValidaUpdateCobranza(CobGeneralCobranza generalCobranza);

    }
}
