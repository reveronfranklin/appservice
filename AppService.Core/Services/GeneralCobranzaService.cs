﻿using AppService.Core.CustomEntities;
using AppService.Core.Entities;
using AppService.Core.Exceptions;
using AppService.Core.Interfaces;
using AppService.Core.QueryFilters;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Core.Services
{
    public class GeneralCobranzaService: IGeneralCobranzaService
    {
        private readonly IUnitOfWork  _unitOfWork;

        private readonly PaginationOptions _paginationOptions;

        public GeneralCobranzaService(IUnitOfWork unitOfWork, IOptions<PaginationOptions> options)
        {
            _unitOfWork = unitOfWork;
            _paginationOptions = options.Value;
        }


        public async  Task<PagedList<CobGeneralCobranza>> GetGeneralCobranza(GeneralCobranzaQueryFilter filters)
        {

            filters.PageNumber = filters.PageNumber == 0 ? _paginationOptions.DefaultPageNumber : filters.PageNumber;
            filters.PageSize = filters.PageSize == 0 ? _paginationOptions.DefaultPageSize : filters.PageSize;

            var cobranzas = await _unitOfWork.GeneralCobranzaRepository.ListCobranzaPorUsuario(filters);


            cobranzas = cobranzas.OrderByDescending(x => x.FechaTransaccion);



            if (filters.IdCliente != null)
            {
                cobranzas = cobranzas.Where(x => x.IdCliente == filters.IdCliente);
            }
                   
            
            if (filters.IdBanco != null)
            {
                cobranzas = cobranzas.Where(x => x.IdBanco == filters.IdBanco);
             
            }
           
            if (filters.IdTipoTransaccion != null)
            {
                cobranzas = cobranzas.Where(x => x.IdTipoTransaccion == filters.IdTipoTransaccion);
             
            }
            
            if (filters.FechaTransaccion != null)
            {
                cobranzas = cobranzas.Where(x => x.FechaTransaccion.ToShortDateString() == filters.FechaTransaccion?.ToShortDateString());
             
            }


            if (filters.UsuarioRegistro != null)
            {
                cobranzas = cobranzas.Where(x => x.UsuarioRegistro == filters.UsuarioRegistro);
             
            }

            if (filters.Documento >0)
            {
                cobranzas = cobranzas.Where(x => x.Documento == filters.Documento);

            }


            

            var pagedCobranzas = PagedList<CobGeneralCobranza>.Create(cobranzas, filters.PageNumber, filters.PageSize);

            return pagedCobranzas;
        }

        public async Task<CobGeneralCobranza> GetGeneralCobranza(long documento)
        {
            return await _unitOfWork.GeneralCobranzaRepository.GetById(documento);
        }
        public async Task<Metadata> ValidaInsertCobranza(CobGeneralCobranza generalCobranza)
        {
            Metadata metadata = new Metadata();

            var user = await _unitOfWork.SegUsuarioRepository.GetUser(generalCobranza.UsuarioRegistro);
            if (user == null)
            {
              
                metadata.IsValid = false;
                metadata.Message = "usuario no existe";
            }

            var existenDocumentosPendientes = await _unitOfWork.GeneralCobranzaRepository.ExisteCobranzaPendienteEnviar(generalCobranza);
            if (existenDocumentosPendientes != null)
            {
                //throw new BusinessException("Este cliente ya tiene un RC abierto pendiente por enviar N° " + existenDocumentosPendientes.Documento.ToString() + ".Agregue la cobranza a este RC.Si es necesario grabar otro RC a este cliente, envíe el que está pendiente a la administradora.");
                metadata.IsValid = false;
                metadata.Message = "Este cliente ya tiene un RC abierto pendiente por enviar N° " + existenDocumentosPendientes.Documento.ToString() + ".Agregue la cobranza a este RC.Si es necesario grabar otro RC a este cliente, envíe el que está pendiente a la administradora.";
            }

          
            return metadata;
        }


        public async Task<Metadata> ValidaUpdateCobranza(CobGeneralCobranza generalCobranza)
        {
            Metadata metadata = new Metadata();

            metadata.IsValid = true;
            metadata.Message = "";
            var cambiaRcRu = await _unitOfWork.GeneralCobranzaRepository.RCRUYaTieneCobranzaGrabada(generalCobranza.Documento);
            if (cambiaRcRu != null  && generalCobranza.IdTipoTransaccion == "RET")
            {
                //throw new BusinessException("Este cliente ya tiene un RC abierto pendiente por enviar N° " + existenDocumentosPendientes.Documento.ToString() + ".Agregue la cobranza a este RC.Si es necesario grabar otro RC a este cliente, envíe el que está pendiente a la administradora.");
                metadata.IsValid = false;
                metadata.Message = "Este recibo ya tiene cobranza registrada, para cambiarlo a retención debe eliminarla.";
            
            }

            var cambiaRet = await _unitOfWork.GeneralCobranzaRepository.RETYaTieneCobranzaGrabada(generalCobranza.Documento);
            if (cambiaRet != null && generalCobranza.IdTipoTransaccion != "RET")
            {
                //throw new BusinessException("Este cliente ya tiene un RC abierto pendiente por enviar N° " + existenDocumentosPendientes.Documento.ToString() + ".Agregue la cobranza a este RC.Si es necesario grabar otro RC a este cliente, envíe el que está pendiente a la administradora.");
                metadata.IsValid = false;
                metadata.Message = "Este recibo ya tiene retenciones registradas, para cambiarla a cobranza debe eliminarla.";
            }


            return metadata;
        }


        public async Task<CobGeneralCobranza> InsertGeneralCobranza(CobGeneralCobranza generalCobranza)
        {

            var user = await _unitOfWork.SegUsuarioRepository.GetUser(generalCobranza.UsuarioRegistro);
            if (user== null)
            {
                throw new Exception("usuario no existe");
            }

            var existenDocumentosPendientes = await _unitOfWork.GeneralCobranzaRepository.ExisteCobranzaPendienteEnviar(generalCobranza);
            if (existenDocumentosPendientes!=null)
            {
                throw new BusinessException("Este cliente ya tiene un RC abierto pendiente por enviar N° " + existenDocumentosPendientes.Documento.ToString()+ ".Agregue la cobranza a este RC.Si es necesario grabar otro RC a este cliente, envíe el que está pendiente a la administradora.");
            }

            await _unitOfWork.GeneralCobranzaRepository.Add(generalCobranza);
            await _unitOfWork.SaveChangesAsync();
            return generalCobranza;
        }


        public async Task<CobGeneralCobranza> UpdateGeneralCobranza(CobGeneralCobranza generalCobranza)
        {

            var cobranza = await GetGeneralCobranza(generalCobranza.Documento);
            if (cobranza== null)
            {
                throw new Exception("Documento No existe");
            }

       

            cobranza.IdCliente = generalCobranza.IdCliente;
            cobranza.IdMtrTipoMoneda = generalCobranza.IdMtrTipoMoneda;
            cobranza.MontoTransaccion = generalCobranza.MontoTransaccion;
            cobranza.IdTipoTransaccion = generalCobranza.IdTipoTransaccion;
            cobranza.IdBanco = generalCobranza.IdBanco;
            cobranza.FechaTransaccion = generalCobranza.FechaTransaccion;
            cobranza.NumReferencia = generalCobranza.NumReferencia;
            cobranza.EmailCliente = generalCobranza.EmailCliente;
            cobranza.FechaRegistro = generalCobranza.FechaRegistro;
            cobranza.UsuarioRegistro = generalCobranza.UsuarioRegistro;

            await  _unitOfWork.GeneralCobranzaRepository.Update(cobranza);
            await _unitOfWork.SaveChangesAsync();

            return await GetGeneralCobranza(generalCobranza.Documento);
            
        }

        public async Task<bool> DeleteGeneralCobranza(long documento)
        {
           await _unitOfWork.GeneralCobranzaRepository.Delete(documento);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

      

      

      
    }
}
