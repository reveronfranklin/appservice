﻿using AppService.Core.CustomEntities;
using AppService.Core.Entities;
using AppService.Core.Interfaces;
using AppService.Core.QueryFilters;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Core.Services
{
    public class MtrClienteService: IMtrClienteService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly PaginationOptions _paginationOptions;

        public MtrClienteService(IUnitOfWork unitOfWork, IOptions<PaginationOptions> options)
        {
            _unitOfWork = unitOfWork;
            _paginationOptions = options.Value;
        }


        public async Task<IEnumerable<MtrCliente>> ListClientesPorUsuario(MtrClienteQueryFilter filter)
        {

            filter.PageNumber = filter.PageNumber == 0 ? _paginationOptions.DefaultPageNumber : filter.PageNumber;
            filter.PageSize = filter.PageSize == 0 ? _paginationOptions.DefaultPageSize : filter.PageSize;

            var clientes= await _unitOfWork.MtrClienteRepository.ListClientesPorUsuario(filter);
            clientes = clientes.OrderBy(x => x.Nombre);

           

            var pagedclientes = PagedList<MtrCliente>.Create(clientes, filter.PageNumber, filter.PageSize);

            return pagedclientes;
            
        }

        public async Task<MtrCliente> GetById(string id)
        {
            return await _unitOfWork.MtrClienteRepository.GetById(id);
        }

    }
}
