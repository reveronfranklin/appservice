﻿using AppService.Core.CustomEntities;
using AppService.Core.Entities;
using AppService.Core.Exceptions;
using AppService.Core.Interfaces;
using AppService.Core.QueryFilters;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Core.Services
{
    public class CobAdjuntosCobranzaService: ICobAdjuntosCobranzaService 
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly PaginationOptions _paginationOptions;

        public CobAdjuntosCobranzaService(IUnitOfWork unitOfWork, IOptions<PaginationOptions> options)
        {
            _unitOfWork = unitOfWork;
            _paginationOptions = options.Value;
        }

        //desde aqui los metodos que voy a utilizar...
        public PagedList<CobAdjuntosCobranza> GetCobAdjuntosCobranza(AdjuntosCobranzaFilter filters)
        {

            filters.PageNumber = filters.PageNumber == 0 ? _paginationOptions.DefaultPageNumber : filters.PageNumber;
            filters.PageSize = filters.PageSize == 0 ? _paginationOptions.DefaultPageSize : filters.PageSize;

            var adjuntosCob = _unitOfWork.CobAdjuntosCobranzaRepository.GetAll();
                       
            if (filters.Documento != null)
            {
                adjuntosCob = adjuntosCob.Where(x => x.Documento == filters.Documento);
            }

            var pagedResult = PagedList<CobAdjuntosCobranza>.Create(adjuntosCob, filters.PageNumber, filters.PageSize);

            return pagedResult;
        }


    }
}
