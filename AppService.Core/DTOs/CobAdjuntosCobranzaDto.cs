﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppService.Core.DTOs
{
    public class CobAdjuntosCobranzaDto
    {        
        public long Documento { get; set; }
        public short IdTipoDocumento { get; set; }
        public string NombreArchivo { get; set; }

        //public byte[] Archivo { get; set; }

        public string IdUsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }

        public string  DescripcionTipoDocumento { get; set; }

        public string Ruta { get; set; }    //añadido por alberto

        public string Link { get; set; }

    }
}
