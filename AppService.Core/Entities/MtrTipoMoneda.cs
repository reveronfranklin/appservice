﻿using System;
using System.Collections.Generic;

namespace AppService.Core.Entities
{
    public partial class MtrTipoMoneda
    {
        public MtrTipoMoneda()
        {
            CobGeneralCobranza = new HashSet<CobGeneralCobranza>();
        }

        public long Id { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<CobGeneralCobranza> CobGeneralCobranza { get; set; }
    }
}
