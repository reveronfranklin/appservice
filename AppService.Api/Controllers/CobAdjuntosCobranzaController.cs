﻿using AppService.Api.Responses;
using AppService.Core.CustomEntities;
using AppService.Core.DTOs;
using AppService.Core.Entities;
using AppService.Core.Interfaces;
using AppService.Core.QueryFilters;
using AppService.Core.Services;
using AppService.Infrastructure.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace AppService.Api.Controllers
{
    //Comentario  para ver si en el git status lo refleja como modificado. 1707

    //TODO Habilitar Authorize (siguiente linea)
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CobAdjuntosCobranzaController : ControllerBase
    {
        private readonly ICobAdjuntosCobranzaService _cobAdjuntosCobranzaService;
        private readonly IMapper _mapper;
        private readonly IUriService _uriService;
        private readonly IOfdTipoDocumentoService _ofdTipoDocumentoService;
        private readonly PaginationOptions _paginationOptions;

        //Constructor
        public CobAdjuntosCobranzaController(ICobAdjuntosCobranzaService cobAdjuntosCobranzaService, 
                                            IMapper mapper, 
                                            IUriService uriService,
                                            IOptions<PaginationOptions> options,
                                            IOfdTipoDocumentoService ofdTipoDocumentoService)
        {
            _cobAdjuntosCobranzaService = cobAdjuntosCobranzaService;
            _mapper = mapper;
            _uriService = uriService;
           _ofdTipoDocumentoService = ofdTipoDocumentoService;
           _paginationOptions = options.Value;
        }

        /// <summary>
        /// Retorna los datos de adjuntos cobranza
        /// </summary>
        /// <param name="filters">Filtros a aplicar AdjuntosCobranzaFilter</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ApiResponse<IEnumerable<CobAdjuntosCobranzaDto>>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCobAdjuntosCobranza([FromQuery] AdjuntosCobranzaFilter filters)
        {
            PagedList<CobAdjuntosCobranza> cobranzaAdjuntos = _cobAdjuntosCobranzaService.GetCobAdjuntosCobranza(filters);

            IEnumerable<CobAdjuntosCobranzaDto> adjuntosCobranzaDto = _mapper.Map<IEnumerable<CobAdjuntosCobranzaDto>>(cobranzaAdjuntos);

            List<CobAdjuntosCobranzaDto> result = new List<CobAdjuntosCobranzaDto>();
            foreach (var item in adjuntosCobranzaDto)
            {
                var tipoDocumento = await _ofdTipoDocumentoService.GetById(item.IdTipoDocumento);
                item.DescripcionTipoDocumento = tipoDocumento.NombreDocumento;
                result.Add(item);
            }

            Metadata metadata = new Metadata
            {
                TotalCount = cobranzaAdjuntos.TotalCount,
                PageSize = cobranzaAdjuntos.PageSize,
                CurrentPage = cobranzaAdjuntos.CurrentPage,
                TotalPage = cobranzaAdjuntos.TotalPage,
                HasNextPage = cobranzaAdjuntos.HasNextPage,
                HasPreviousPage = cobranzaAdjuntos.HasPreviousPage,
                NextPageUrl = "",  // _uriService.GetGeneralCobranzaPaginationUri(filters, "api/GeneralCobranzas").ToString(),
                PreviousPageUrl = ""  //_uriService.GetGeneralCobranzaPaginationUri(filters, "api/GeneralCobranzas").ToString(),
            };

            //ApiResponse<IEnumerable<CobGeneralCobranzaDto>> response = new ApiResponse<IEnumerable<CobGeneralCobranzaDto>>(generalCobranzasDtos);
            var response = new ApiResponse<IEnumerable<CobAdjuntosCobranzaDto>>(result)
            {
                Meta = metadata
            };

            return Ok(response);
        }

        //
        /// <summary>
        /// Retorna los datos de adjuntos cobranza
        /// Filtros a aplicar AdjuntosCobranzaFilter, si en el objeto incluye documento, 
        /// se realizara el filtro de los datos
        /// </summary>
        /// <param name="filters">Filtros a aplicar AdjuntosCobranzaFilter, si en el objeto incluye documento, se realizara el filtro de los datos</param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ApiResponse<IEnumerable<CobAdjuntosCobranzaDto>>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllAdjuntosCobranza(AdjuntosCobranzaFilter filters)
        {
           
            
            PagedList<CobAdjuntosCobranza> listaAdjuntos;

            listaAdjuntos = _cobAdjuntosCobranzaService.GetCobAdjuntosCobranza(filters);
                       
            IEnumerable<CobAdjuntosCobranzaDto> adjuntosCobranzaDto = _mapper.Map<IEnumerable<CobAdjuntosCobranzaDto>>(listaAdjuntos);


            List<CobAdjuntosCobranzaDto> result = new List<CobAdjuntosCobranzaDto>();
            foreach (var item in adjuntosCobranzaDto)
            {
                var tipoDocumento = await _ofdTipoDocumentoService.GetById(item.IdTipoDocumento);
                item.DescripcionTipoDocumento = tipoDocumento.NombreDocumento;
                item.Link = _paginationOptions.UrlGetFiles + item.NombreArchivo;
                result.Add(item);
            }


            Metadata metadata = new Metadata
            {
                TotalCount = listaAdjuntos.TotalCount,
                PageSize = listaAdjuntos.PageSize,
                CurrentPage = listaAdjuntos.CurrentPage,
                TotalPage = listaAdjuntos.TotalPage,
                HasNextPage = listaAdjuntos.HasNextPage,
                HasPreviousPage = listaAdjuntos.HasPreviousPage,
                NextPageUrl = "",       // _uriService.GetGeneralCobranzaPaginationUri(filters, "").ToString(),
                PreviousPageUrl = ""    // _uriService.GetGeneralCobranzaPaginationUri(filters, "").ToString(),
            };

            var response = new ApiResponse<IEnumerable<CobAdjuntosCobranzaDto>>(result)
            {
                Meta = metadata
            };
                       
            return Ok(response);
        }
    }
}