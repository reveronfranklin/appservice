﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AppService.Api.Responses;
using AppService.Core.DTOs;
using AppService.Core.Entities;
using AppService.Core.Interfaces;
using AppService.Core.QueryFilters;
using AppService.Infrastructure.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AppService.Api.Controllers
{

    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class MtrBancosController : ControllerBase
    {


        private readonly IMtrBancosService _mtrBancosService;

        private readonly IMapper _mapper;
        private readonly IUriService _uriService;
        public MtrBancosController(IMtrBancosService mtrBancosService , IMapper mapper, IUriService uriService)
        {
            _mtrBancosService = mtrBancosService;
            _mapper = mapper;
            _uriService = uriService;
        }



        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> GetBancos(MtrBancosQueryFilter filters)
        {


            IEnumerable<MtrBancos> bancos = await _mtrBancosService.GetAll(filters);

            IEnumerable<MtrBancosDto> bancosDto = _mapper.Map<IEnumerable<MtrBancosDto>>(bancos);

          
            

            var response = new ApiResponse<IEnumerable<MtrBancosDto>>(bancosDto)
            {
                Meta = null
            };



            return Ok(response);

        }


    }
}