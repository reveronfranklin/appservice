﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AppService.Api.Responses;
using AppService.Core.CustomEntities;
using AppService.Core.DTOs;
using AppService.Core.Entities;
using AppService.Core.Interfaces;
using AppService.Core.QueryFilters;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AppService.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class MtrClientesController : ControllerBase
    {
        private readonly IMtrClienteService _mtrClienteService;
        private readonly IMapper _mapper;

        public MtrClientesController(IMtrClienteService mtrClienteService, IMapper mapper)
        {
            _mtrClienteService = mtrClienteService;
            _mapper = mapper;
        }



        /// <summary>
        /// Retorna Lista de Clientes
        /// </summary>
        /// <param name="filters">Filtros a aplicar</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ApiResponse<IEnumerable<MtrClienteDto>>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMtrClientes([FromQuery] MtrClienteQueryFilter filters)
        {


            IEnumerable<MtrCliente> clientes =await  _mtrClienteService.ListClientesPorUsuario(filters);

            IEnumerable<MtrClienteDto> clientesDtos = _mapper.Map<IEnumerable<MtrClienteDto>>(clientes);



            ApiResponse<IEnumerable<MtrClienteDto>> response = new ApiResponse<IEnumerable<MtrClienteDto>>(clientesDtos);
          

        

            return Ok(response);

        }


        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> ListClientesPorUsuario(MtrClienteQueryFilter filters)
        {


            IEnumerable<MtrCliente> clientes = await _mtrClienteService.ListClientesPorUsuario(filters);

            IEnumerable<MtrClienteDto> clientesDtos = _mapper.Map<IEnumerable<MtrClienteDto>>(clientes);

            if (filters.Codigo!=null && filters.Codigo != "")
            {
                clientesDtos = clientesDtos.Where(x => x.Descripcion.ToLower().Trim().Contains(filters.Codigo.ToLower().Trim()));

            }

            ApiResponse<IEnumerable<MtrClienteDto>> response = new ApiResponse<IEnumerable<MtrClienteDto>>(clientesDtos);




            return Ok(response);
           
        }

    }
}