﻿using AppService.Api.Responses;
using AppService.Core.CustomEntities;
using AppService.Core.DTOs;
using AppService.Core.Entities;
using AppService.Core.Interfaces;
using AppService.Core.QueryFilters;
using AppService.Infrastructure.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace AppService.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class GeneralCobranzasController : ControllerBase
    {
        private readonly IGeneralCobranzaService _generalCobranzaService;

        private readonly IMapper _mapper;
        private readonly IUriService _uriService;
        private readonly IMtrClienteService _mtrClienteService;
        private readonly IMtrVendedorService _mtrVendedorService;

        public GeneralCobranzasController(IGeneralCobranzaService generalCobranzaService,
                                            IMapper mapper,
                                            IUriService uriService,
                                            IMtrClienteService mtrClienteService,
                                            IMtrVendedorService mtrVendedorService)
        {
            _generalCobranzaService = generalCobranzaService;
            _mapper = mapper;
            _uriService = uriService;
            _mtrClienteService = mtrClienteService;
            _mtrVendedorService = mtrVendedorService;
        }

        /// <summary>
        /// Retorna los datos generales de Cobros
        /// </summary>
        /// <param name="filters">Filtros a aplicar</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ApiResponse<IEnumerable<CobGeneralCobranzaDto>>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetGeneralCobranzas([FromQuery] GeneralCobranzaQueryFilter filters)
        {


            PagedList<CobGeneralCobranza> generalCobranzas = await _generalCobranzaService.GetGeneralCobranza(filters);

            IEnumerable<CobGeneralCobranzaDto> generalCobranzasDtos = _mapper.Map<IEnumerable<CobGeneralCobranzaDto>>(generalCobranzas);



            Metadata metadata = new Metadata
            {
                TotalCount = generalCobranzas.TotalCount,
                PageSize = generalCobranzas.PageSize,
                CurrentPage = generalCobranzas.CurrentPage,
                TotalPage = generalCobranzas.TotalPage,
                HasNextPage = generalCobranzas.HasNextPage,
                HasPreviousPage = generalCobranzas.HasPreviousPage,
                NextPageUrl = _uriService.GetGeneralCobranzaPaginationUri(filters, "api/GeneralCobranzas").ToString(),
                PreviousPageUrl = _uriService.GetGeneralCobranzaPaginationUri(filters, "api/GeneralCobranzas").ToString(),
            };

            //ApiResponse<IEnumerable<CobGeneralCobranzaDto>> response = new ApiResponse<IEnumerable<CobGeneralCobranzaDto>>(generalCobranzasDtos);
            ApiResponse<IEnumerable<CobGeneralCobranzaDto>> response = new ApiResponse<IEnumerable<CobGeneralCobranzaDto>>(generalCobranzasDtos)
            {
                Meta = metadata
            };

            Response.Headers.Add("x-Pagination", JsonConvert.SerializeObject(metadata));

            return Ok(response);

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetGeneralCobranza(int id)
        {


            CobGeneralCobranza generalCobranza = await _generalCobranzaService.GetGeneralCobranza(id);
            CobGeneralCobranzaDto generalCobranzasDto = _mapper.Map<CobGeneralCobranzaDto>(generalCobranza);

            ApiResponse<CobGeneralCobranzaDto> response = new ApiResponse<CobGeneralCobranzaDto>(generalCobranzasDto);
            return Ok(response);
        }


        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> Insert(CobGeneralCobranzaDto generalCobranzaDto)
        {

            CobGeneralCobranza generalCobranza = _mapper.Map<CobGeneralCobranza>(generalCobranzaDto);

            var metaValidacion = await _generalCobranzaService.ValidaInsertCobranza(generalCobranza);
            if (metaValidacion.IsValid)
            {
                CobGeneralCobranza result = await _generalCobranzaService.InsertGeneralCobranza(generalCobranza);

                generalCobranzaDto = _mapper.Map<CobGeneralCobranzaDto>(result);

            }
  
            ApiResponse<CobGeneralCobranzaDto> response = new ApiResponse<CobGeneralCobranzaDto>(generalCobranzaDto)
            {
                Meta = metaValidacion
            }; 
            return Ok(response);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> Update(CobGeneralCobranzaDto generalCobranzaDto)
        {

            CobGeneralCobranza generalCobranza = _mapper.Map<CobGeneralCobranza>(generalCobranzaDto);


            var metaValidacion = await _generalCobranzaService.ValidaUpdateCobranza(generalCobranza);
            if (metaValidacion.IsValid)
            {
                CobGeneralCobranza result = await _generalCobranzaService.UpdateGeneralCobranza(generalCobranza);
                generalCobranzaDto = _mapper.Map<CobGeneralCobranzaDto>(result);

            }

          

            ApiResponse<CobGeneralCobranzaDto> response = new ApiResponse<CobGeneralCobranzaDto>(generalCobranzaDto)
            {
                Meta = metaValidacion
            };
            return Ok(response);

        }


        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> GetAllGeneralCobranzas(GeneralCobranzaQueryFilter filters)
        {


            PagedList<CobGeneralCobranza> generalCobranzas = await _generalCobranzaService.GetGeneralCobranza(filters);

            IEnumerable<CobGeneralCobranzaDto> generalCobranzasDtos = _mapper.Map<IEnumerable<CobGeneralCobranzaDto>>(generalCobranzas);

            List<CobGeneralCobranzaDto> result = new List<CobGeneralCobranzaDto>();
            foreach (CobGeneralCobranzaDto item in generalCobranzasDtos)
            {
                MtrCliente cliente = await _mtrClienteService.GetById(item.IdCliente);
                MtrVendedor vendedor = await _mtrVendedorService.GetById(cliente.Vendedor1);
                item.NombreCliente = cliente.Nombre.Trim();
                item.IdCliente = item.IdCliente.Trim();
                item.FechaTransaccionString = item.FechaTransaccion.ToShortDateString();
                item.NombreVendedor = vendedor.Nombre;
                decimal decimalValue = item.MontoTransaccion;
                item.MontoTransaccionString = string.Format("{0:N}", decimalValue); // 1,234,567.00
                result.Add(item);
            }

            Metadata metadata = new Metadata
            {
                TotalCount = generalCobranzas.TotalCount,
                PageSize = generalCobranzas.PageSize,
                CurrentPage = generalCobranzas.CurrentPage,
                TotalPage = generalCobranzas.TotalPage,
                HasNextPage = generalCobranzas.HasNextPage,
                HasPreviousPage = generalCobranzas.HasPreviousPage,
                NextPageUrl = _uriService.GetGeneralCobranzaPaginationUri(filters, "api/GeneralCobranzas").ToString(),
                PreviousPageUrl = _uriService.GetGeneralCobranzaPaginationUri(filters, "api/GeneralCobranzas").ToString(),
            };

            //ApiResponse<IEnumerable<CobGeneralCobranzaDto>> response = new ApiResponse<IEnumerable<CobGeneralCobranzaDto>>(generalCobranzasDtos);
            ApiResponse<IEnumerable<CobGeneralCobranzaDto>> response = new ApiResponse<IEnumerable<CobGeneralCobranzaDto>>(result)
            {
                Meta = metadata
            };

            Response.Headers.Add("x-Pagination", JsonConvert.SerializeObject(metadata));

            return Ok(response);

        }



        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> GetGeneralCobranzasByDocumento(GeneralCobranzaQueryFilter filters)
        {


            CobGeneralCobranza generalCobranzas = await _generalCobranzaService.GetGeneralCobranza(filters.Documento);

            CobGeneralCobranzaDto generalCobranzasDtos = _mapper.Map<CobGeneralCobranzaDto>(generalCobranzas);

            CobGeneralCobranzaDto result = new CobGeneralCobranzaDto();

            MtrCliente cliente = await _mtrClienteService.GetById(generalCobranzasDtos.IdCliente);
            MtrVendedor vendedor = await _mtrVendedorService.GetById(cliente.Vendedor1);
            generalCobranzasDtos.NombreCliente = cliente.Nombre.Trim();
            generalCobranzasDtos.IdCliente = generalCobranzasDtos.IdCliente.Trim();
            generalCobranzasDtos.FechaTransaccionString = generalCobranzasDtos.FechaTransaccion.ToShortDateString();
            generalCobranzasDtos.NombreVendedor = vendedor.Nombre;
            decimal decimalValue = generalCobranzasDtos.MontoTransaccion;
            generalCobranzasDtos.MontoTransaccionString = string.Format("{0:N}", decimalValue); // 1,234,567.00



       
            ApiResponse<CobGeneralCobranzaDto> response = new ApiResponse<CobGeneralCobranzaDto>(generalCobranzasDtos)
            {
                Meta = null
            };

  

            return Ok(response);

        }


      

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {


            bool result = await _generalCobranzaService.DeleteGeneralCobranza(id);
            ApiResponse<bool> response = new ApiResponse<bool>(result);
            return Ok(response);
        }

    }
}