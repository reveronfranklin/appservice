using AppService.Core.CustomEntities;
using AppService.Core.Interfaces;
using AppService.Core.Services;
using AppService.Infrastructure.Data;
using AppService.Infrastructure.Filters;
using AppService.Infrastructure.Interfaces;
using AppService.Infrastructure.Repositories;
using AppService.Infrastructure.Services;
using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace AppService.Api
{
    public class Startup
    {


        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //services.AddCors();








            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddControllers(options =>
            {
                options.Filters.Add<GlobalExceptionFilter>();
            }).AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            }).ConfigureApiBehaviorOptions(options =>
            {
                // options.SuppressModelStateInvalidFilter = true;
            });

            services.Configure<PaginationOptions>(Configuration.GetSection("Pagination"));

            services.AddDbContext<RRDContext>(options =>

               options.UseSqlServer(Configuration.GetConnectionString("rrdConecction"))

                );

            services.AddTransient<ICobAdjuntosCobranzaService, CobAdjuntosCobranzaService>();
            services.AddTransient<IGeneralCobranzaService, GeneralCobranzaService>();
            services.AddTransient<ISegUsuarioService, SegUsuarioService>();
            services.AddTransient<IMtrClienteService, MtrClienteService>();
            services.AddTransient<ICobTipoTransaccionService, CobTipoTransaccionService>();
            services.AddTransient<IOfdTipoDocumentoService, OfdTipoDocumentoService>();
            services.AddTransient<IMtrVendedorService, MtrVendedorService>();
            services.AddTransient<IMtrOficinaService, MtrOficinaService>();
            services.AddTransient<IMtrBancosService, MtrBancosService>();
            services.AddTransient<IMtrTipoMonedaService, MtrTipoMonedaService>();



            services.AddScoped(typeof(IRepository<>), typeof(BaseRepository<>));
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<IUriService>(provider =>
            {
                IHttpContextAccessor accesor = provider.GetRequiredService<IHttpContextAccessor>();
                HttpRequest request = accesor.HttpContext.Request;
                string absoluteUri = string.Concat(request.Scheme, "://", request.Host.ToUriComponent());
                return new UriService(absoluteUri);
            });


            services.AddSwaggerGen(doc =>
            {
                doc.SwaggerDoc("v1", new OpenApiInfo { Title = "App Service Api", Version = "v1" });
                string xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                string xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                doc.IncludeXmlComments(xmlPath);

            });


            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["Authentication:Issuer"],
                    ValidAudience = Configuration["Authentication:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Authentication:SecrectKey"])),
                };
            });



            services.AddCors(o => o.AddPolicy("charppolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddMvc(options =>
           {
               options.Filters.Add<ValidationFilter>();
               options.ReturnHttpNotAcceptable = false;
           }).AddFluentValidation(options =>
           {

               options.RegisterValidatorsFromAssemblies(AppDomain.CurrentDomain.GetAssemblies());
           });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {


            
            


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {

                options.SwaggerEndpoint("/swagger/v1/swagger.json", "App Service API V1");
                options.RoutePrefix = string.Empty;
            });

            app.UseRouting();

            app.UseCors("charppolicy");


            app.UseAuthentication();

            app.UseAuthorization();



            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
