import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms'
import { UploaderComponent } from './uploader/uploader.component';

@NgModule({
    declarations: [
        UploaderComponent
  ],
  imports: [
      CommonModule,
      CommonModule,
      IonicModule,      
      FormsModule,
      ReactiveFormsModule      
    ],
    exports: [
        UploaderComponent
    ]  
})
export class ComponentModule { }
