import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GeneralService } from './general.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeneralCobranzaService {
  basePath: string;
  accionPath: string;
  controller: string;
  constructor(private http: HttpClient, private gensvc: GeneralService) {
    this.basePath = gensvc.basePath;
  }

  GetAllGeneralCobranzas(data): Observable<any> {
    this.controller = 'GeneralCobranzas/';
    this.accionPath = "GetAllGeneralCobranzas";

    return this.http.post<any>(this.basePath + this.controller + this.accionPath, JSON.stringify(data)).pipe(
    );
  }
  GetAllGeneralCobranzasByDocumento(data): Observable<any> {
    this.controller = 'GeneralCobranzas/';
    this.accionPath = "GetGeneralCobranzasByDocumento";

    return this.http.post<any>(this.basePath + this.controller + this.accionPath, JSON.stringify(data)).pipe(
    );
  }

  UpdateGeneralCobranzas(data): Observable<any> {
    this.controller = 'GeneralCobranzas/';
    this.accionPath = "Update";

    return this.http.post<any>(this.basePath + this.controller + this.accionPath, JSON.stringify(data)).pipe(
    );
  }

  InsertGeneralCobranzas(data): Observable<any> {
    this.controller = 'GeneralCobranzas/';
    this.accionPath = "Insert";

    return this.http.post<any>(this.basePath + this.controller + this.accionPath, JSON.stringify(data)).pipe(
    );
  }


}
