import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GeneralService } from './general.service';
import { Observable } from 'rxjs';
import { MtrClienteDto } from '../models/mtr-cliente-dto';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  basePath: string;
  accionPath: string;
  controller: string;
  constructor(private http: HttpClient, private gensvc: GeneralService) {
    this.basePath = gensvc.basePath;
  }

  ListClientesPorUsuario(data): Observable<any> {
    this.controller = 'MtrClientes/';
    this.accionPath = "ListClientesPorUsuario";

    return this.http.post<any>(this.basePath + this.controller + this.accionPath, JSON.stringify(data)).pipe(
    );
  }

}
