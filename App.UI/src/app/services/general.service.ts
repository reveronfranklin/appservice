import { Injectable } from '@angular/core';
import { IUsuario } from '../interfaces/iusuario';
import { ToastController, AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  basePath: string;
  basePathHub: string;

   // objeto usuario de tipo IUsuario
   usuario: IUsuario;

   constructor(public toastCtrl: ToastController,
               public alertCtrl: AlertController) {
        
       //development mode
        this.basePath = 'https://localhost:44314/api/';
        this.basePathHub = 'https://localhost:44314/';        
    
       //from device test
       //this.basePath = 'https://192.168.88.246:44314/api/';
       //this.basePathHub = 'https://192.168.88.246:44314/'; 
   
   }

    // Establace valores individualmente en localstorage
    SetItem(clave: string, valor: any): void {

      localStorage.setItem(clave, valor);

  }

  // resetea el objeto usuario
  KillUsuario(): void {

      // inicializo valores en localstorage
      this.SetItem('User', '');
      this.SetItem('Password', '');
      this.SetItem('Token', '');
      this.SetItem('Validate', 'false');
      this.SetItem('Role', '');
    

      // resetea objeto usuario
      this.ResetUsuario();
      
  }

  // resetea el objeto usuario
  ResetUsuario(): void {

      // reseteo objeto usuario
      this.usuario = {
          user: '',
          password: '',
          token: '',
          validate: 'false',
          role: '',
          Page:1,
          ResultsCount:0,
          TotalPages:1,
           PageSize:20,
           PageNumber:1,
          
      };

     


  }

  // Devuelve objeto usuario con datos de localstorage
  GetUsuario(): IUsuario {
      this.ResetUsuario();

      this.usuario = {
          user: localStorage.getItem('User'),
          password: '',
          token: localStorage.getItem('Token'),
          validate: localStorage.getItem('Validate'),
          role: localStorage.getItem('Role'),
          Page:1,
          ResultsCount:0,
          TotalPages:1,
          PageSize:20,
          PageNumber:1,
      };

      return this.usuario;
  }



}
