
export class MtrBancosDto {


    recnum:number;
    codigo:string;
    nombre:string;
    codContable:string;
    aplicaImp:string;
    traAsociada:string;
    prodGenerico:string;
    traJde:string;
    flagActivo:boolean;
    flagIngreso:boolean;
    flagEgreso:boolean;
    idTipoCuentaDestino:number;


}
