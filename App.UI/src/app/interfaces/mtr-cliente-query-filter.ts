export interface MtrClienteQueryFilter {

    usuario?:string;
    oficina?:string;
    vendedor?:string;
    codigo?:string;
    
     //Paginacion
     PageSize:number;
    
     PageNumber:number;
}
