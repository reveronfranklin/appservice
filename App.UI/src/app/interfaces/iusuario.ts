export interface IUsuario {

    user: string ;
    password: string ;
    token: string;
    validate: string;
    id?: string;
    role: string;
    Page:number;
    ResultsCount:number;
    TotalPages:number;


     //Paginacion
     PageSize:number;

     PageNumber:number;
}
