import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  usuario = {
    User: '',
    Password: '',
  };
  constructor(public loginService: LoginService,
    public activateRoute: ActivatedRoute,
    public router: Router,public gs: GeneralService) { }

    ngOnInit() {
      //TODO poner en blanco para produccion
      this.usuario.User = 'Rr105841';
      this.usuario.Password = 'mvc4-2014';
  }

  onSubmitLogin() {

    console.log(this.usuario);

    this.loginService.login(this.usuario).subscribe(response => {
       console.log(response); 
       console.log("OK",response.validate) 
        if (response.validate) {

            
            
            localStorage.setItem('Validate', 'true');
            localStorage.setItem('Token', response.token);
            localStorage.setItem('User', response.user);
   
            localStorage.setItem('Role', response.role);
            console.log(response);
                   
            this.router.navigateByUrl('/menu/main');
        
        } else {
            
            localStorage.setItem('Validate', 'false');
            this.gs.KillUsuario();
            console.log(response);
        
        }

    });
  
  }

}
