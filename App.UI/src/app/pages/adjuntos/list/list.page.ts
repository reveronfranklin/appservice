import { Component, OnInit } from '@angular/core';
import { CobAdjuntosCobranzaService } from '../../../services/cob-adjuntos-cobranza.service';
import { GeneralService } from '../../../services/general.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AdjuntosCobranzaFilter } from '../../../interfaces/adjuntos-cobranza-filter';
import { CobGeneralCobranzaDto } from 'src/app/models/cob-general-cobranza-dto';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
    styleUrls: ['./list.page.scss'],  
})
export class ListPage implements OnInit {

    filter: AdjuntosCobranzaFilter;
    itemcobGeneralCobranzaDto: CobGeneralCobranzaDto;

    //recibe el numero de doc enviado
    documentoId: string

    adjuntos = []    

    constructor(
                private cobac: CobAdjuntosCobranzaService,
                private activateRoute: ActivatedRoute,
                private router: Router,
                private gensvc: GeneralService){
        
        this.activateRoute.queryParams.subscribe(params => {
            
            //Lee objeto enviado desde Listado de Cobranzas
            if (this.router.getCurrentNavigation().extras.state)
            {               
                this.itemcobGeneralCobranzaDto = this.router.getCurrentNavigation().extras.state.user;                            
                
                this.showHeader();

                console.log("El objeto recibido es:")
                console.log(this.itemcobGeneralCobranzaDto)
                console.log(this.itemcobGeneralCobranzaDto.documento)
                console.log("@@@@@@@")
                
            }          
            
        });

    }

    ngOnInit() {
       
        this.filter = {
            documento: +this.itemcobGeneralCobranzaDto.documento,
            pageSize: 100,
            pageNumber: 1,
        };

        this.cobac.GetAdjuntosByDcumento(this.filter).subscribe(response => {

            console.log(response)

            this.adjuntos = response.data

        });
    }

    showHeader() { 

    }

}
