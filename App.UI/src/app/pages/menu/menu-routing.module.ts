import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';
import { AuthGuard } from '../../guards/auth.guard';
const routes: Routes = [
    {
        path: '',
        redirectTo: '/menu/main',
        pathMatch: 'full'
    },
    {
        path: '',
        component: MenuPage,
        children: [
            {
                path: 'main', loadChildren: () => import('./../main/main.module').then(m => m.MainPageModule), canActivate: [AuthGuard]
            },
            {
                path: 'adjuntos-list', loadChildren: () => import('./../adjuntos/list/list.module').then(m => m.ListPageModule), canActivate: [AuthGuard]
            },
            {
                path: 'general-cobranza-list',
                loadChildren: () => import('./../cobranza/general-cobranza-list/general-cobranza-list.module').then(m => m.GeneralCobranzaListPageModule)
            },
            {
                path: 'cobranza-edit/:id',
                loadChildren: () => import('./../cobranza/cobranza-edit/cobranza-edit.module').then(m => m.CobranzaEditPageModule)
            },

            {
                path: 'grabacion-cobranza-list/:id',
                loadChildren: () => import('./../cobranza/grabacion-cobranza-list/grabacion-cobranza-list.module').then(m => m.GrabacionCobranzaListPageModule)
            }

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class MenuPageRoutingModule { }
