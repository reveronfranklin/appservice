import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

    pages = [
        {
            title: 'Main',
            url: '/menu/main',
            icon: 'home'
        },
        {
            title: 'Cobranzas',
            children: [
                {
                    title: 'Cobranzas',
                    url: '/menu/general-cobranza-list',
                    icon: 'home'
                }
            ]
        },
        {
            title: 'Adjuntos',
            children: [
                {
                    title: 'Consulta de Adjuntos',
                    url: '/menu/adjuntos-list',
                    icon: 'home'
                }
            ]
        }
    ]    
  
    constructor() { }

    ngOnInit() {
    }

}
