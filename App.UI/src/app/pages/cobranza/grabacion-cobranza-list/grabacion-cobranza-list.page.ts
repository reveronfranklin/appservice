import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GeneralService } from '../../../services/general.service';

@Component({
  selector: 'app-grabacion-cobranza-list',
  templateUrl: './grabacion-cobranza-list.page.html',
  styleUrls: ['./grabacion-cobranza-list.page.scss'],
})
export class GrabacionCobranzaListPage implements OnInit {
  
  documento: number;
  parametroId: string;
  constructor(public activateRoute: ActivatedRoute,
    public router: Router,
    public gs: GeneralService) { }

  ngOnInit() {

    this.parametroId = 'id';
    this.documento = +this.activateRoute.snapshot.params[this.parametroId];
    console.log("Id Item Recibido", this.documento);
  }

}
