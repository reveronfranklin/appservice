import { Component, OnInit, ViewChild } from '@angular/core';
import { GeneralService } from '../../../services/general.service';
import { MtrOficinaServiceService } from '../../../services/mtr-oficina-service.service';
import { IUsuario } from '../../../interfaces/iusuario';
import { MtrOficinaQueryFilter } from '../../../interfaces/mtr-oficina-query-filter';
import { MtrOficinaDto, Oficina } from '../../../models/mtr-oficina-dto';
import { GeneralCobranzaService } from '../../../services/general-cobranza.service';
import { GeneralCobranzaQueryFilter } from '../../../interfaces/general-cobranza-query-filter';
import { CobGeneralCobranzaDto } from '../../../models/cob-general-cobranza-dto';
import { IonInfiniteScroll } from '@ionic/angular';
import { MtrVendedorDto } from '../../../models/mtr-vendedor-dto';
import { MtrVendedorService } from '../../../services/mtr-vendedor.service';
import { MtrVendedorQueryFilter } from '../../../interfaces/mtr-vendedor-query-filter';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-general-cobranza-list',
  templateUrl: './general-cobranza-list.page.html',
  styleUrls: ['./general-cobranza-list.page.scss'],
})
export class GeneralCobranzaListPage implements OnInit {


  usuario: IUsuario;
  mtrOficinaQueryFilter:MtrOficinaQueryFilter;
  generalCobranzaQueryFilter: GeneralCobranzaQueryFilter;

  documento:number=0;

  mtrOficinaDto:MtrOficinaDto[]=[];
  mtrOficinaSelected:string="";

  pageSize = 30;
  page=0;
  
  oficinas: Oficina[] = [ {codigo: '', descripcion: 'Select'}];

 cobranzaList= [ {IdCliente: '', MontoTransaccion: 0}];

  cobGeneralCobranzaDto: CobGeneralCobranzaDto[]=[];

  mtrVendedoresDto:MtrVendedorDto[]=[];
  mtrVendedorQueryFilter:MtrVendedorQueryFilter;
  mtrVendedorSelected:string="";

  itemcobGeneralCobranzaDto: CobGeneralCobranzaDto;

  @ViewChild(IonInfiniteScroll,{static:true}) infiniteScroll:IonInfiniteScroll;
 
  constructor(private gs:GeneralService,
              private mtrOficinaService:MtrOficinaServiceService,
              private cobranzaService:GeneralCobranzaService,
              private mtrVendedorService:MtrVendedorService,
              private router: Router) {
                
                this.usuario = this.gs.GetUsuario();

              }

  async ngOnInit() {

    this.mtrOficinaQueryFilter = {
      Usuario: this.usuario.user,
      PageSize:100,
      PageNumber:1,
    };

    this.mtrOficinaService.GetAllOficinas(  this.mtrOficinaQueryFilter).subscribe(resp => { 
      this.mtrOficinaDto=resp.data;
      console.log(this.mtrOficinaDto);
      for (let clave of this.mtrOficinaDto){
       
        let  oficina: Oficina =  {codigo: clave['codOficina'], descripcion: clave['nomOficina']};
      
        this.oficinas.push(oficina);

      }
    });


    await this.getVededores();

    await this.refresh();

  }

  async onChangeOfic(event){
    console.log("oficina Seleccionada",event.target.value);
    this.mtrOficinaSelected=event.target.value;
    console.log("Cargando.....",this.mtrOficinaSelected);
    this.mtrVendedorSelected="";
    this.cobGeneralCobranzaDto=[];
    await this.getVededores();
  

  }

  async onChangeVend(event){
    console.log("Vendedor Seleccionada",event.target.value);
    this.mtrVendedorSelected=event.target.value;
    console.log("Cargando.....",this.mtrVendedorSelected);
    this.cobGeneralCobranzaDto=[];
    await this.refresh();
  }


async getVededores(){
  this.mtrVendedorQueryFilter = {
    usuario: this.usuario.user,
    oficina:+this.mtrOficinaSelected
  };
  this.mtrVendedorService.ListVendedoresPorUsuario(  this.mtrVendedorQueryFilter).subscribe(resp => { 
    this.mtrVendedoresDto=resp.data;
    console.log("Vendedores",this.mtrVendedoresDto);
   
  });
}


  

  onChangeRC(event){
    this.documento=+event.target.value;
    this.cobGeneralCobranzaDto=[];
    this.refresh();
  }

  onClickAdd(){
    this.router.navigate(['menu/cobranza-edit/' + 1]);
  }
  
  onClickDetail(item){
      console.log(item);

      this.itemcobGeneralCobranzaDto=item;
      let navigationExtras: NavigationExtras = {
        state: {
          user: this.itemcobGeneralCobranzaDto
        }
      };
     // this.router.navigate(['menu/cobranza-edit'], navigationExtras);

      this.router.navigate(['menu/cobranza-edit/' + this.itemcobGeneralCobranzaDto.documento]);

  }
  onClickDetailAdjunto(item){
    console.log(item);

    this.itemcobGeneralCobranzaDto=item;
    let navigationExtras: NavigationExtras = {
      state: {
        user: this.itemcobGeneralCobranzaDto
      }
    };
    this.router.navigate(['menu/adjuntos-list'], navigationExtras);


   
}

  

  async refresh(event?){
    

 
  
    this.generalCobranzaQueryFilter = {
      UsuarioConectado: this.usuario.user,
      IdOficina:this.mtrOficinaSelected,
      vendedor:this.mtrVendedorSelected,
      documento:this.documento,
      PageSize:this.pageSize,
      PageNumber: this.page,
    };
    
    console.log("Query Filter", this.generalCobranzaQueryFilter);
    


    this.cobranzaService.GetAllGeneralCobranzas(this.generalCobranzaQueryFilter)
    .subscribe(resp => {
     
      if(resp.data.counts<=0){
        //event.target.complete();
        this.infiniteScroll.disabled=true;
        event.target.complete();
        return;
      }
      console.log(resp);
      
      if(this.page>1){
        
        this.cobGeneralCobranzaDto=this.cobGeneralCobranzaDto.concat(resp['data']);
       
        /*for (let clave of this.cobGeneralCobranzaDto){
       
          let  cobro =  {IdCliente: clave['IdCliente'], MontoTransaccion: clave['MontoTransaccion']};
        
          this.cobranzaList.push(cobro);
  
        }*/

        
      }else{
        this.cobGeneralCobranzaDto=resp.data;
        /*for (let clave of this.cobGeneralCobranzaDto){
       
          let  cobro =  {IdCliente: clave['IdCliente'], MontoTransaccion: clave['MontoTransaccion']};
        
          this.cobranzaList.push(cobro);
  
        }*/
      }
      if(event){
        event.target.complete();
      }
      
       console.log("Cobranza",this.cobGeneralCobranzaDto);
     
  
   
    });

  }

  loadMore(event){
    this.page++;
    console.log('Cargando datos...',this.page)
    
    this.refresh(event);
  }

}
