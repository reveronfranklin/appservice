import { Component, OnInit } from '@angular/core';
import { CobGeneralCobranzaDto } from '../../../models/cob-general-cobranza-dto';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { GeneralService } from '../../../services/general.service';
import { IUsuario } from '../../../interfaces/iusuario';
import { ModalController, ToastController } from '@ionic/angular';
import { ClienteListPage } from '../../clientes/cliente-list/cliente-list.page';
import { SearchClientePage } from '../../clientes/search-cliente/search-cliente.page';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms'
import { debounceTime } from 'rxjs/operators'
import { CobTipoTransaccionService } from '../../../services/cob-tipo-transaccion.service';
import { CobTipoTransaccionDto } from '../../../models/cob-tipo-transaccion-dto';
import { CobTipoTransaccionQueryFilter } from '../../../interfaces/cob-tipo-transaccion-query-filter';
import { MtrBancosQueryFilter } from '../../../interfaces/mtr-bancos-query-filter';
import { MtrBancosService } from '../../../services/mtr-bancos.service';
import { MtrBancosDto } from '../../../models/mtr-bancos-dto';
import { MtrTipoMonedaDto } from '../../../models/mtr-tipo-moneda-dto';
import { MtrTipoMonedaService } from '../../../services/mtr-tipo-moneda.service';
import { MtrTipoMonedaQueryFilter } from '../../../interfaces/mtr-tipo-moneda-query-filter';
import { GeneralCobranzaQueryFilter } from '../../../interfaces/general-cobranza-query-filter';
import { GeneralCobranzaService } from '../../../services/general-cobranza.service';


@Component({
  selector: 'app-cobranza-edit',
  templateUrl: './cobranza-edit.page.html',
  styleUrls: ['./cobranza-edit.page.scss'],
})
export class CobranzaEditPage implements OnInit {

  itemcobGeneralCobranzaDto = new CobGeneralCobranzaDto();
  listCobTipoTransaccionDto: CobTipoTransaccionDto[] = [];

  cobTipoTransaccionQueryFilter: CobTipoTransaccionQueryFilter;
  usuario: IUsuario;
  titulo: string;

  //codigoClienteCtrl = new FormControl('', [Validators.required,Validators.maxLength(6),Validators.minLength(6)]);

  codigo: string = "";
  nombreCliente: string = "";


  tipoTransaccionSelected: CobTipoTransaccionDto;


  mtrBancosQueryFilter: MtrBancosQueryFilter;
  listMtrBancosDto: MtrBancosDto[] = [];

  form: FormGroup;
  listMtrTipoMonedasDto: MtrTipoMonedaDto[] = [];
  mtrTipoMonedaQueryFilter: MtrTipoMonedaQueryFilter;
  mtrTipoMonedasDtoSelected: MtrTipoMonedaDto;
  descripcionMoneda = "";


  generalCobranzaQueryFilter: GeneralCobranzaQueryFilter;


  documento: number;
  parametroId: string;

  mensaje="";
 
  constructor(public activateRoute: ActivatedRoute,
    public router: Router,
    public gs: GeneralService,
    private modalCtrl: ModalController,
    private formBuilder: FormBuilder,
    private cobTipoTransaccionService: CobTipoTransaccionService,
    private mtrBancosService: MtrBancosService,
    private MtrTipoMonedaService: MtrTipoMonedaService,
    private generalCobranzaService: GeneralCobranzaService,
    public toastController:ToastController) {

    this.buildForm();

  }

  async openToast(message,color){
    const toast = await this.toastController.create({
      message,
      duration:5000,
      position:'top',
      color

    });
    toast.present();
  }

  async ngOnInit() {
    this.usuario = this.gs.GetUsuario();
    this.titulo = "Editar Cobro"
    this.parametroId = 'id';
    this.documento = +this.activateRoute.snapshot.params[this.parametroId];
    console.log("Id Item Recibido", this.documento);
   

    this.itemcobGeneralCobranzaDto.documento=this.documento;
    if(this.documento==1){
      this.CargarCombos();
      this.idMonedaField.setValue(0)
    }else{
      this.refresh();
    }
   

  }

  private CargarCombos() {
    //Moneda
    this.mtrTipoMonedaQueryFilter = {
      id: 0,
      descripcion: ""
    };
    this.MtrTipoMonedaService.ListMonedas(this.mtrTipoMonedaQueryFilter).subscribe(respMoneda => {
      this.listMtrTipoMonedasDto = respMoneda.data;
    });
    //Tipo de Transaccion
    this.cobTipoTransaccionQueryFilter = {
      searchText: "",
    };

    this.cobTipoTransaccionService.ListCobTipoTransaccion(this.cobTipoTransaccionQueryFilter).subscribe(respTipo => {
      this.listCobTipoTransaccionDto = respTipo.data;

    });
    //Banco

    this.mtrBancosQueryFilter = {
      codigo: "",
      nombre: "",
      idTipoTransaccion: "",
    };

    this.GetListBancos(this.mtrBancosQueryFilter);

  }

  private refresh() {
    this.generalCobranzaQueryFilter = {
      UsuarioConectado: this.usuario.user,
      IdOficina: "",
      vendedor: "",
      documento: this.documento,
      PageSize: 0,
      PageNumber: 0,
    };
    this.generalCobranzaService.GetAllGeneralCobranzasByDocumento(this.generalCobranzaQueryFilter).subscribe(resp => {
      console.log(resp.data);


      //Moneda
      this.mtrTipoMonedaQueryFilter = {
        id: 0,
        descripcion: ""
      };
      this.MtrTipoMonedaService.ListMonedas(this.mtrTipoMonedaQueryFilter).subscribe(respMoneda => {
        this.listMtrTipoMonedasDto = respMoneda.data;
        this.mtrTipoMonedasDtoSelected = this.listMtrTipoMonedasDto.find(i => i.id == resp.data.idMtrTipoMoneda);
        this.descripcionMoneda = this.mtrTipoMonedasDtoSelected.descripcion;

      });

      //Tipo de Transaccion
      this.cobTipoTransaccionQueryFilter = {
        searchText: "",
      };

      this.cobTipoTransaccionService.ListCobTipoTransaccion(this.cobTipoTransaccionQueryFilter).subscribe(respTipo => {
        this.listCobTipoTransaccionDto = respTipo.data;
        this.tipoTransaccionSelected = this.listCobTipoTransaccionDto.find(i => i.idTipoTransaccion == resp.data.idTipoTransaccion);
      });


      this.codigoClienteField.setValue(resp.data.idCliente);
      this.fechaTransaccionField.setValue(resp.data.fechaTransaccion);
      this.montoTransaccionField.setValue(resp.data.montoTransaccion);
      this.emailField.setValue(resp.data.emailCliente);
      this.idTipoTransaccionField.setValue(resp.data.idTipoTransaccion);
      this.nombreCliente = resp.data.nombreCliente;
      this.codigo = resp.data.idCliente;
      this.idBancoField.setValue(resp.data.idBanco);
      this.idMonedaField.setValue(resp.data.idMtrTipoMoneda)
      this.numReferenciaField.setValue(resp.data.numReferencia);

      //Banco

      this.mtrBancosQueryFilter = {
        codigo: "",
        nombre: "",
        idTipoTransaccion: resp.data.idTipoTransaccion,
      };

      this.GetListBancos(this.mtrBancosQueryFilter);

    });

  }

  private buildForm() {
    this.form = this.formBuilder.group({
      codigoCliente: ['', [Validators.required, Validators.maxLength(6), Validators.minLength(6)]],
      idMoneda: [1, [Validators.required]],
      montoTransaccion: [0, [Validators.required, Validators.min(1)]],
      idTipoTransaccion: ["", [Validators.required]],
      idBanco: ["", [Validators.required]],
      fechaTransaccion: ["", [Validators.required]],
      numReferencia: ["", [Validators.required]],

      email: ["", [Validators.required, Validators.email]],
    });

    /* this.form.valueChanges
     .pipe(
       debounceTime(500)
     )
     .subscribe(value=> {
       console.log(value);
     });*/
  }


  save(event: Event) {
    event.preventDefault();
    const value = this.form.value;
    if (this.form.valid) {
      console.log(value);
      
      this.itemcobGeneralCobranzaDto.documento=this.documento;
      this.itemcobGeneralCobranzaDto.idCliente= this.codigoClienteField.value;
      this.itemcobGeneralCobranzaDto.idMtrTipoMoneda=this.idMonedaField.value;
      this.itemcobGeneralCobranzaDto.montoTransaccion=this.montoTransaccionField.value;
      this.itemcobGeneralCobranzaDto.idTipoTransaccion=this.idTipoTransaccionField.value;
      this.itemcobGeneralCobranzaDto.idBanco=this.idBancoField.value;
      this.itemcobGeneralCobranzaDto.fechaTransaccion=this.fechaTransaccionField.value;
      this.itemcobGeneralCobranzaDto.numReferencia=this.numReferenciaField.value;
      this.itemcobGeneralCobranzaDto.EmailCliente=this.emailField.value;
      let date = new Date();
      this.itemcobGeneralCobranzaDto.FechaRegistro=date;
      this.itemcobGeneralCobranzaDto.usuarioRegistro=this.usuario.user;


      if(this.documento==1){
        this.generalCobranzaService.InsertGeneralCobranzas(this.itemcobGeneralCobranzaDto).subscribe(resp => {
          console.log(resp);
           
          if(!resp.meta.isValid){

           
             
              this.openToast(resp.meta.message,'danger');
            
          }else{
              this.openToast("Recibo Guardado Satisfactoriamente",'success');
            
           
          }

        });
      }else{
        console.log("Objeto a guardar",this.itemcobGeneralCobranzaDto)
         this.generalCobranzaService.UpdateGeneralCobranzas(this.itemcobGeneralCobranzaDto).subscribe(resp => {
          console.log(resp);
          if(!resp.meta.isValid){
           
            this.openToast(resp.meta.message,'danger');
          }else{
            
            this.openToast("Recibo Guardado Satisfactoriamente",'success');
          }
       
          });
      }

    }

    //console.log(this.codigoClienteCtrl.value);
  }

  getCodigoCliente(event: Event) {
    event.preventDefault();
    //console.log(this.codigoClienteCtrl.value);
  }

  get codigoClienteField() {
    return this.form.get('codigoCliente')
  }

  get fechaTransaccionField() {
    return this.form.get('fechaTransaccion')
  }

  get montoTransaccionField() {
    return this.form.get('montoTransaccion')
  }
  get emailField() {
    return this.form.get('email')
  }
  get idTipoTransaccionField() {
    return this.form.get('idTipoTransaccion')
  }

  get idBancoField() {
    return this.form.get('idBanco')
  }
  get numReferenciaField() {
    return this.form.get('numReferencia')
  }
  get idMonedaField() {
    return this.form.get('idMoneda')
  }

  async onBuscarCliente() {

    const modal = await this.modalCtrl.create({
      component: SearchClientePage,
      componentProps: {
        userConectado: this.usuario.user
      }
    });
    await modal.present();

    const { data } = await modal.onDidDismiss();
    console.log("datos retornados por el modal", data);
    this.codigo = data.clienteSeleccionado;
    this.nombreCliente = data.nombreCliente;
    //this.codigoClienteField().setvalue(this.codigo);
    this.codigoClienteField.setValue(this.codigo);
  }

  async onChangeTipoTransaccion(event) {
    console.log("Tipo Transaccion Seleccionada", event.target.value);
    this.mtrBancosQueryFilter = {
      codigo: "",
      nombre: "",
      idTipoTransaccion: event.target.value,
    };

    this.GetListBancos(this.mtrBancosQueryFilter);
  }

  onChangeBanco(event) {
    console.log("Banco Seleccionado", event.target.value);
  }

  onChangeMoneda(event) {
    console.log("Moneda Seleccionado", event.target.value);
    this.mtrTipoMonedasDtoSelected = this.listMtrTipoMonedasDto.find(i => i.id == event.target.value);
    this.descripcionMoneda = this.mtrTipoMonedasDtoSelected.descripcion;

  }

  GetListBancos(mtrBancosQueryFilter: MtrBancosQueryFilter) {

    this.mtrBancosService.ListBancos(mtrBancosQueryFilter).subscribe(resp => {
      this.listMtrBancosDto = resp.data;
      console.log("Listad de  Bancos", this.listMtrBancosDto);


    });
  }
  onClickExit() {
    this.gs.KillUsuario();
    this.router.navigate(['/login']);
  }

  onGotoGrabacionCobranza(){
 

   
    let navigationExtras: NavigationExtras = {
      state: {
        user: this.itemcobGeneralCobranzaDto
      }
    };
   // this.router.navigate(['menu/cobranza-edit'], navigationExtras);

    this.router.navigate(['menu/grabacion-cobranza-list/' + this.itemcobGeneralCobranzaDto.documento]);

  }



}
