﻿using AppService.Core.Entities;
using AppService.Core.Interfaces;
using AppService.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Infrastructure.Repositories
{
    public class MtrBancosRepository: IMtrBancosRepository
    {

        private readonly RRDContext _context;

        public MtrBancosRepository(RRDContext context)
        {
            _context = context;
        }

        public IEnumerable<MtrBancos> GetAll()
        {

            return _context.MtrBancos.AsEnumerable();

        }

        public async Task<MtrBancos> GetById(short id)
        {
            return await _context.MtrBancos.FindAsync(id);
        }

        public async Task<MtrBancos> GetByCodigo(string id)
        {
            return await _context.MtrBancos.Where(x=>x.Codigo==id).FirstOrDefaultAsync();
        }

        public async Task Add(MtrBancos entity)
        {
            await _context.MtrBancos.AddAsync(entity);


        }



        public void Update(MtrBancos entity)
        {
            _context.MtrBancos.Update(entity);

        }

        public async Task Delete(string id)
        {
            MtrBancos entity = await GetByCodigo(id);
            _context.MtrBancos.Remove(entity);

        }


        public async Task<List<MtrBancos>> GetBancos(string Id)
        {
            var bancos = await (from ban in _context.MtrBancos 
                                join trancuen in _context.MtrTipoTransaccionCuentaDestino on ban.IdTipoCuentaDestino equals trancuen.IdTipoCuentaDestino 
                                where ban.FlagActivo == true && ban.FlagIngreso == true && trancuen.IdTipoTransaccion == Id select ban).ToListAsync();

            //bancos.Add(new MtrBancos { Codigo = "", Nombre = "[Seleccione Cuenta]" });

            return bancos.OrderBy(x => x.Nombre).ToList();



        }

    }
}
