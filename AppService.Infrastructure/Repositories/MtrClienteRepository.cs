﻿using AppService.Core.Entities;
using AppService.Core.Interfaces;
using AppService.Core.QueryFilters;
using AppService.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppService.Infrastructure.Repositories
{
    public class MtrClienteRepository : IMtrClienteRepository
    {
        private readonly RRDContext _context;

        public MtrClienteRepository(RRDContext context) 
        {
            _context = context;
        }

        public IEnumerable<MtrCliente> GetAll(string usuario)
        {

            return _context.MtrCliente.AsEnumerable();

        }

        public async Task<MtrCliente> GetById(string id)
        {

            return await _context.MtrCliente.Where(x=> x.Codigo==id).FirstOrDefaultAsync();

        }

        public async Task<IEnumerable<MtrCliente>> ListClientesPorUsuario(MtrClienteQueryFilter filter)
        {
            List<MtrCliente> result = new List<MtrCliente>();
            string usuario = filter.Usuario;

            var vendedor = await _context.MtrVendedor.Where(x => x.Codigo == usuario).FirstOrDefaultAsync();
            if (vendedor!=null)
            {
                result =  await _context.MtrCliente.Where(x => x.Vendedor1 == usuario && x.FlagInactivo!= "X").ToListAsync();
            }

            var supervisor = await _context.MtrVendedor.Where(x => x.Supervisor == usuario).FirstOrDefaultAsync();
            if (supervisor != null)
            {

                result = await (from u in _context.MtrCliente
                                     join ur in _context.MtrVendedor on u.Vendedor1 equals ur.Codigo
                                    
                                     where ur.Supervisor == usuario & u.FlagInactivo !="X" 
                                     select u).ToListAsync();

            }

            var cobRolCobranza = await _context.CobRolCobranza.Where(x => x.IdUsuario == usuario).ToListAsync();
            if (cobRolCobranza.Count>0)
            {

                foreach (var item in cobRolCobranza)
                {
                    var clie = await _context.MtrCliente.Where(x => x.OficinaVenta == item.IdOficina.ToString() && x.FlagInactivo != "X").ToListAsync();
                    foreach (var itemcli in clie)
                    {
                        result.Add(itemcli);
                    }
                  
                }
            }

            var usuarioOficina = await _context.MtrUsuarioOficina.Where(x => x.Usuario == usuario).ToListAsync();
            if (usuarioOficina.Count > 0)
            {

                foreach (var item in usuarioOficina)
                {
                    var clie = await _context.MtrCliente.Where(x => x.OficinaVenta == item.Oficina.ToString() && x.FlagInactivo != "X").ToListAsync();
                    foreach (var itemcli in clie)
                    {
                        result.Add(itemcli);
                    }

                }
            }

            if (filter.Oficina!= null)
            {
                result =result.Where(x => x.OficinaVenta.Trim() == filter.Oficina.Trim()).ToList();
            }
            if (filter.Vendedor != null)
            {
                result = result.Where(x => x.Vendedor1.ToLower().Trim() == filter.Vendedor.ToLower().Trim()).ToList();
            }


           /* if (filter.Codigo != null && filter.Codigo!= "")
            {
                result = result.Where(x => x.Codigo.ToLower().Trim().Contains(filter.Codigo.ToLower().Trim()) ).ToList();
            }*/
            return result;

        }

    }
}
