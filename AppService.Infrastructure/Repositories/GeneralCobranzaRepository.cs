﻿using AppService.Core.DTOs;
using AppService.Core.Entities;
using AppService.Core.Interfaces;
using AppService.Core.QueryFilters;
using AppService.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Infrastructure.Repositories
{
    public class GeneralCobranzaRepository :  ICobGeneralCobranzaRepository
    {
        private readonly RRDContext _context;

        public GeneralCobranzaRepository(RRDContext context)
        {
            _context = context;
        }


        public IEnumerable<CobGeneralCobranza> GetAll()
        {
            return _context.CobGeneralCobranza.AsEnumerable();
        }

        public async Task<CobGeneralCobranza> GetById(long id)
        {
            return await _context.CobGeneralCobranza.FindAsync(id);
        }

        public async Task Add(CobGeneralCobranza entity)
        {
            await _context.CobGeneralCobranza.AddAsync(entity);


        }



        public async Task Update(CobGeneralCobranza entity)
        {
           
            _context.CobGeneralCobranza.Update(entity);

        }

        public async Task Delete(long id)
        {
            CobGeneralCobranza entity = await GetById(id);
            _context.CobGeneralCobranza.Remove(entity);

        }

        public async Task<CobGeneralCobranza> ExisteCobranzaPendienteEnviar(CobGeneralCobranza generalCobranza)
        {
            var row = await  _context.CobGeneralCobranza.Where(x => x.IdCliente == generalCobranza.IdCliente && x.FlagEnviado == false && x.FlagAnulado == false).FirstOrDefaultAsync();
            return row ;
        }
        public async Task<CobGrabacionCobranzas> RCRUYaTieneCobranzaGrabada(long documento)
        {
            var row = await _context.CobGrabacionCobranzas.Where(x => x.Documento == documento && (x.Transaccion == "RC" || x.Transaccion == "RU")).FirstOrDefaultAsync();
            return row;
        }

        public async Task<CobGrabacionCobranzas> RETYaTieneCobranzaGrabada(long documento)
        {
            var row = await _context.CobGrabacionCobranzas.Where(x => x.Documento == documento && x.Transaccion == "RE" ).FirstOrDefaultAsync();
            return row;
        }


      

        public async Task<IEnumerable<CobGeneralCobranza>> ListCobranzaPorUsuario(GeneralCobranzaQueryFilter filter)
        {
            List<CobGeneralCobranza> result = new List<CobGeneralCobranza>();
            string usuario = filter.UsuarioConectado;

            var vendedor = await _context.MtrVendedor.Where(x => x.Codigo == usuario).FirstOrDefaultAsync();
            if (vendedor != null)
            {
               

                var cobresult = await (from u in _context.CobGeneralCobranza
                                       join ur in _context.MtrCliente on u.IdCliente equals ur.Codigo

                                       where ur.Vendedor1 == usuario
                                       select u).ToListAsync();
                foreach (var itemcobresult in cobresult)
                {
                    result.Add(itemcobresult);
                }


            }

            var supervisor = await _context.MtrVendedor.Where(x => x.Supervisor == usuario).FirstOrDefaultAsync();
            if (supervisor != null)
            {


                result = await (from u in _context.CobGeneralCobranza
                                join ur in _context.MtrVendedor on u.UsuarioRegistro equals ur.Codigo

                                where ur.Supervisor == usuario 
                                select u).ToListAsync();

            }

            List<MtrUsuarioOficina> usuarioOficina = new List<MtrUsuarioOficina>();
            if (filter.IdOficina==null)
            {
                usuarioOficina = await _context.MtrUsuarioOficina.Where(x => x.Usuario == usuario).ToListAsync();
            }
            else
            {
                int id = 0;

                Int32.TryParse(filter.IdOficina, out id);
                usuarioOficina = await _context.MtrUsuarioOficina.Where(x => x.Usuario == usuario && x.Oficina == id).ToListAsync();
            }
            
            if (usuarioOficina.Count > 0)
            {

                foreach (var item in usuarioOficina)
                {



                    var cobresult = await (from u in _context.CobGeneralCobranza
                                    join ur in _context.MtrCliente on u.IdCliente equals ur.Codigo

                                    where ur.OficinaVenta == item.Oficina.ToString()
                                    select u).ToListAsync();


                    //var clie = await _context.MtrCliente.Where(x => x.OficinaVenta == item.Oficina.ToString() && x.FlagInactivo != "X").ToListAsync();
                    foreach (var itemcobresult in cobresult)
                    {
                        result.Add(itemcobresult);
                    }

                }
            }

            if (filter.Vendedor != "")
            {

               
                var cobresult = await (from u in _context.CobGeneralCobranza
                                       join ur in _context.MtrCliente on u.IdCliente equals ur.Codigo

                                       where ur.Vendedor1 == filter.Vendedor.ToString()
                                       select u).ToListAsync();
                foreach (var itemcobresult in cobresult)
                {
                    result.Add(itemcobresult);
                }
            }



            return result;

        }

    }
}
