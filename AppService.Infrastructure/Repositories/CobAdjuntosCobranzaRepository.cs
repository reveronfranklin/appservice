﻿using AppService.Core.DTOs;
using AppService.Core.Entities;
using AppService.Core.Interfaces;
using AppService.Core.QueryFilters;
using AppService.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Infrastructure.Repositories
{
    public class CobAdjuntosCobranzaRepository: ICobAdjuntosCobranzaRepository
    {
        private readonly RRDContext _context;

        public CobAdjuntosCobranzaRepository(RRDContext context) 
        {
            _context = context;
        }

        public IEnumerable<CobAdjuntosCobranza> GetAll()
        {
            return _context.CobAdjuntosCobranza.AsEnumerable();
        }



    }
}
