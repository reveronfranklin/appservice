﻿using AppService.Core.Entities;
using AppService.Core.Interfaces;
using AppService.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppService.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly RRDContext _context;

        //new
        private readonly ICobAdjuntosCobranzaRepository _cobAdjuntosCobranzaRepository;

        private readonly ICobGeneralCobranzaRepository _cobGeneralCobranzaRepository;

        public readonly ISegUsuarioRepository _segUsuarioRepository;

        public readonly IMtrClienteRepository _mtrClienteRepository;

        public readonly ICobTipoTransaccionRepository _cobTipoTransaccionRepository;
       
        public readonly IOfdTipoDocumentoRepository _ofdTipoDocumentoRepository;
        
        public readonly IMtrOficinaRepository _mtrOficinaRepository;


        public readonly IMtrVendedorRepository _mtrVendedorRepository;

        public readonly IMtrBancosRepository _mtrBancosRepository;

        public readonly IMtrTipoMonedaRepository _mtrTipoMonedaRepository;

        public UnitOfWork(RRDContext context)
        {
            _context = context;
        }
        

        //new
        public ICobAdjuntosCobranzaRepository CobAdjuntosCobranzaRepository => _cobAdjuntosCobranzaRepository ?? new CobAdjuntosCobranzaRepository(_context);

        public ICobGeneralCobranzaRepository GeneralCobranzaRepository => _cobGeneralCobranzaRepository ?? new GeneralCobranzaRepository(_context);

        public ISegUsuarioRepository SegUsuarioRepository => _segUsuarioRepository ?? new SegUsuarioRepository(_context);

        public IMtrClienteRepository MtrClienteRepository => _mtrClienteRepository ?? new MtrClienteRepository(_context);

        public ICobTipoTransaccionRepository CobTipoTransaccionRepository => _cobTipoTransaccionRepository ?? new CobTipoTransaccionRepository(_context);

        public IOfdTipoDocumentoRepository OfdTipoDocumentoRepository => _ofdTipoDocumentoRepository ?? new OfdTipoDocumentoRepository(_context);


        public IMtrOficinaRepository MtrOficinaRepository => _mtrOficinaRepository ?? new MtrOficinaRepository(_context);

        public IMtrVendedorRepository MtrVendedorRepository => _mtrVendedorRepository ?? new MtrVendedorRepository(_context);

        public IMtrBancosRepository MtrBancosRepository => _mtrBancosRepository ?? new MtrBancosRepository(_context);

        public IMtrTipoMonedaRepository MtrTipoMonedaRepository => _mtrTipoMonedaRepository ?? new MtrTipoMonedaRepository(_context);


        public void Dispose()
        {
            if (_context==null)
            {
                _context.Dispose();
            }
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
