﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using AppService.Core.Entities;


namespace AppService.Infrastructure.Data
{
    public partial class RRDContext : DbContext
    {
        public RRDContext()
        {
        }

        public RRDContext(DbContextOptions<RRDContext> options)
            : base(options)
        {
        }

      
        public virtual DbSet<CobAdjuntosCobranza> CobAdjuntosCobranza { get; set; }
        public virtual DbSet<CobAvisosCobro> CobAvisosCobro { get; set; }
        public virtual DbSet<CobAvisosCobroTemp> CobAvisosCobroTemp { get; set; }
        public virtual DbSet<CobBanco> CobBanco { get; set; }
        public virtual DbSet<CobDatosCheque> CobDatosCheque { get; set; }
        public virtual DbSet<CobDescuentoIvaTipoTransaccion> CobDescuentoIvaTipoTransaccion { get; set; }
        public virtual DbSet<CobEstadoDeCuenta> CobEstadoDeCuenta { get; set; }
        public virtual DbSet<CobExcepcion> CobExcepcion { get; set; }
        public virtual DbSet<CobGeneralCobranza> CobGeneralCobranza { get; set; }
        public virtual DbSet<CobGrabacionCobranzas> CobGrabacionCobranzas { get; set; }
        public virtual DbSet<CobIntegridadCobranzasVsWary074> CobIntegridadCobranzasVsWary074 { get; set; }
        public virtual DbSet<CobPagosRetenciones> CobPagosRetenciones { get; set; }
        public virtual DbSet<CobPorcentajesImpuestos> CobPorcentajesImpuestos { get; set; }
        public virtual DbSet<CobRangosVencimiento> CobRangosVencimiento { get; set; }
        public virtual DbSet<CobResumenEstadoCuenta> CobResumenEstadoCuenta { get; set; }
        public virtual DbSet<CobResumenEstadoCuentaTemp> CobResumenEstadoCuentaTemp { get; set; }
        public virtual DbSet<CobRolCobranza> CobRolCobranza { get; set; }
        public virtual DbSet<CobSysfile> CobSysfile { get; set; }
        public virtual DbSet<CobTipoTransaccion> CobTipoTransaccion { get; set; }
        public virtual DbSet<CobTipoTransaccionRespaldoAntesEliminarColettilla> CobTipoTransaccionRespaldoAntesEliminarColettilla { get; set; }
        public virtual DbSet<CobTransacciones> CobTransacciones { get; set; }
        public virtual DbSet<CobValoresRetencionIva> CobValoresRetencionIva { get; set; }
        public virtual DbSet<CobValoresRetenciones> CobValoresRetenciones { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<DataInicial> DataInicial { get; set; }
        public virtual DbSet<DeudorTipoRetencion> DeudorTipoRetencion { get; set; }
        public virtual DbSet<Deudores> Deudores { get; set; }
     
        public virtual DbSet<MtrBancos> MtrBancos { get; set; }
        public virtual DbSet<MtrCliente> MtrCliente { get; set; }
        public virtual DbSet<MtrClienteProspecto> MtrClienteProspecto { get; set; }
        public virtual DbSet<MtrClienteTemp> MtrClienteTemp { get; set; }
        public virtual DbSet<MtrCondicionPago> MtrCondicionPago { get; set; }
        public virtual DbSet<MtrContactos> MtrContactos { get; set; }
        public virtual DbSet<MtrEmpleado> MtrEmpleado { get; set; }
        public virtual DbSet<MtrFechaFeriada> MtrFechaFeriada { get; set; }
        public virtual DbSet<MtrHorarioLaborable> MtrHorarioLaborable { get; set; }
        public virtual DbSet<MtrOficina> MtrOficina { get; set; }
        public virtual DbSet<MtrPerfilCorreo> MtrPerfilCorreo { get; set; }
        public virtual DbSet<MtrProducto> MtrProducto { get; set; }
        public virtual DbSet<MtrProductoBasica> MtrProductoBasica { get; set; }
        public virtual DbSet<MtrProductoOpuesta> MtrProductoOpuesta { get; set; }
        public virtual DbSet<MtrProductoPapeles> MtrProductoPapeles { get; set; }
        public virtual DbSet<MtrTipoCuentaDestino> MtrTipoCuentaDestino { get; set; }
        public virtual DbSet<MtrTipoDocumentoFiscal> MtrTipoDocumentoFiscal { get; set; }
        public virtual DbSet<MtrTipoMoneda> MtrTipoMoneda { get; set; }
        public virtual DbSet<MtrTipoNomina> MtrTipoNomina { get; set; }
        public virtual DbSet<MtrTipoOrden> MtrTipoOrden { get; set; }
        public virtual DbSet<MtrTipoTransaccionCuentaDestino> MtrTipoTransaccionCuentaDestino { get; set; }
        public virtual DbSet<MtrTipoValorRetencion> MtrTipoValorRetencion { get; set; }
        public virtual DbSet<MtrUsuarioOficina> MtrUsuarioOficina { get; set; }
        public virtual DbSet<MtrVendedor> MtrVendedor { get; set; }
        public virtual DbSet<OfdTipoDocumento> OfdTipoDocumento { get; set; }
        public virtual DbSet<SegModulo> SegModulo { get; set; }
        public virtual DbSet<SegPrograma> SegPrograma { get; set; }
        public virtual DbSet<SegRol> SegRol { get; set; }
        public virtual DbSet<SegRolEstacion> SegRolEstacion { get; set; }
        public virtual DbSet<SegRolModulo> SegRolModulo { get; set; }
        public virtual DbSet<SegTicket> SegTicket { get; set; }
        public virtual DbSet<SegUsuario> SegUsuario { get; set; }
        public virtual DbSet<SegUsuarioRol> SegUsuarioRol { get; set; }
        public virtual DbSet<Sysfile> Sysfile { get; set; }
        public virtual DbSet<T006a> T006a { get; set; }
        public virtual DbSet<TempRbfactura> TempRbfactura { get; set; }
        public virtual DbSet<TempReciboCobro> TempReciboCobro { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Server=.;Database=RRD;Integrated Security = true");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
      

        

            modelBuilder.Entity<CobAdjuntosCobranza>(entity =>
            {
                entity.HasKey(e => e.IdAdjunto);

                entity.HasIndex(e => e.Documento)
                    .HasName("IX_CobAdjuntosCobranza");

                entity.HasIndex(e => new { e.Documento, e.IdTipoDocumento })
                    .HasName("IX_CobAdjuntosCobranza_1");

                entity.Property(e => e.Archivo).IsRequired();

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.IdUsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.NombreArchivo)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Ruta)
                    .IsRequired()
                    .HasMaxLength(600);
            });

            modelBuilder.Entity<CobAvisosCobro>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Cob_AvisosCobro");

                entity.HasIndex(e => e.Cliente)
                    .HasName("IX_Cob_AvisosCobro");

                entity.HasIndex(e => e.Fecha)
                    .HasName("IX_Cob_AvisosCobro_1");

                entity.HasIndex(e => e.Procesado)
                    .HasName("IX_Cob_AvisosCobro_2");

                entity.Property(e => e.Archivo).IsRequired();

                entity.Property(e => e.ArchivotTiff).IsRequired();

                entity.Property(e => e.Cliente)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Fecha).HasColumnType("smalldatetime");

                entity.Property(e => e.Html).HasColumnName("HTML");

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.NombreArchivo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Usuario)
                    .IsRequired()
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<CobAvisosCobroTemp>(entity =>
            {
                entity.HasKey(e => e.Cliente);

                entity.ToTable("Cob_AvisosCobro_Temp");

                entity.HasIndex(e => e.Cliente)
                    .HasName("IX_Cob_AvisosCobro_Temp");

                entity.HasIndex(e => e.Usuario)
                    .HasName("IX_Cob_AvisosCobro_Temp_1");

                entity.Property(e => e.Cliente).HasMaxLength(10);

                entity.Property(e => e.Archivo).IsRequired();

                entity.Property(e => e.ArchivotTiff).IsRequired();

                entity.Property(e => e.Html).HasColumnName("HTML");

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.NombreArchivo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Usuario)
                    .IsRequired()
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<CobBanco>(entity =>
            {
                entity.HasKey(e => e.IdBanco);

                entity.Property(e => e.NombreBanco).HasMaxLength(50);
            });

            modelBuilder.Entity<CobDatosCheque>(entity =>
            {
                entity.HasKey(e => e.Documento);

                entity.Property(e => e.Documento).ValueGeneratedNever();

                entity.Property(e => e.FechaCheque).HasColumnType("datetime");

                entity.Property(e => e.IdBanco)
                    .IsRequired()
                    .HasMaxLength(4);

                entity.Property(e => e.IdTipoTransaccion)
                    .IsRequired()
                    .HasMaxLength(4);

                entity.Property(e => e.MontoCheque).HasColumnType("numeric(18, 2)");

                entity.HasOne(d => d.DocumentoNavigation)
                    .WithOne(p => p.CobDatosCheque)
                    .HasForeignKey<CobDatosCheque>(d => d.Documento)
                    .HasConstraintName("FK_CobDatosCheque_CobGeneralCobranza");
            });

            modelBuilder.Entity<CobDescuentoIvaTipoTransaccion>(entity =>
            {
                entity.HasIndex(e => new { e.IdTipoTransaccion, e.BsDesde, e.BsHasta })
                    .HasName("IX_CobDescuentoIvaTipoTransaccion")
                    .IsUnique();

                entity.Property(e => e.BsDesde).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.BsHasta).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.FechaRegistro)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IdTipoTransaccion)
                    .IsRequired()
                    .HasMaxLength(4);

                entity.Property(e => e.PorcDescuento).HasColumnType("numeric(18, 2)");
            });

            modelBuilder.Entity<CobEstadoDeCuenta>(entity =>
            {
                entity.HasIndex(e => e.IdCliente)
                    .HasName("IX_CobEstadoDeCuenta_1");

                entity.HasIndex(e => e.Rpdoc)
                    .HasName("IX_CobEstadoDeCuenta_2");

                entity.HasIndex(e => new { e.Rpkco, e.Rpdct, e.Rpdoc, e.Rpsfx })
                    .HasName("IX_CobEstadoDeCuenta")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BaseImponible)
                    .HasColumnType("numeric(18, 2)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DocumentoSap).HasMaxLength(10);

                entity.Property(e => e.FechaDocumento).HasColumnType("datetime");

                entity.Property(e => e.FechaVencimiento).HasColumnType("datetime");

                entity.Property(e => e.IdCliente).HasMaxLength(50);

                entity.Property(e => e.Iva)
                    .HasColumnType("numeric(18, 2)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MontoOriginal)
                    .HasColumnType("numeric(18, 2)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.OficinaVenta)
                    .HasColumnName("Oficina_Venta")
                    .HasMaxLength(10);

                entity.Property(e => e.Pltyp)
                    .HasColumnName("PLTYP")
                    .HasMaxLength(2)
                    .HasDefaultValueSql("('Z2')");

                entity.Property(e => e.Rpaap)
                    .HasColumnName("RPAAP")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Rpag)
                    .HasColumnName("RPAG")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Rpan8)
                    .HasColumnName("RPAN8")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Rpdct)
                    .IsRequired()
                    .HasColumnName("RPDCT")
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.Rpdoc).HasColumnName("RPDOC");

                entity.Property(e => e.Rpkco)
                    .IsRequired()
                    .HasColumnName("RPKCO")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.Rpsfx)
                    .IsRequired()
                    .HasColumnName("RPSFX")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.Vendedor).HasMaxLength(4);
            });

            modelBuilder.Entity<CobExcepcion>(entity =>
            {
                entity.HasKey(e => e.IdExcepcion);

                entity.HasIndex(e => e.Documento)
                    .HasName("IX_CobExcepcion")
                    .IsUnique();

                entity.Property(e => e.FechaLm)
                    .HasColumnName("FechaLM")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaRegistro).HasColumnType("datetime");

                entity.Property(e => e.Motivo)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.UsuarioRegistro)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.HasOne(d => d.DocumentoNavigation)
                    .WithOne(p => p.CobExcepcion)
                    .HasForeignKey<CobExcepcion>(d => d.Documento)
                    .HasConstraintName("FK_CobExcepcion_CobGeneralCobranza");
            });

            modelBuilder.Entity<CobGeneralCobranza>(entity =>
            {
                entity.HasKey(e => e.Documento);

                entity.HasIndex(e => e.NumReferencia)
                    .HasName("IX_CobGeneralCobranza");

                entity.Property(e => e.Batch).HasDefaultValueSql("((0))");

                entity.Property(e => e.EmailCliente)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FechaAnulado).HasColumnType("datetime");

                entity.Property(e => e.FechaAprobado).HasColumnType("datetime");

                entity.Property(e => e.FechaConfirmado).HasColumnType("datetime");

                entity.Property(e => e.FechaEnviado).HasColumnType("datetime");

                entity.Property(e => e.FechaLm)
                    .HasColumnName("FechaLM")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaLmcxC)
                    .HasColumnName("FechaLMCxC")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaRegistro)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FechaReversado).HasColumnType("datetime");

                entity.Property(e => e.FechaTransaccion).HasColumnType("datetime");

                entity.Property(e => e.FlagAnulado).HasDefaultValueSql("((0))");

                entity.Property(e => e.FlagConfirmado).HasDefaultValueSql("((0))");

                entity.Property(e => e.FlagPagoMas).HasDefaultValueSql("((0))");

                entity.Property(e => e.IdBanco)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.IdCliente)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.IdTipoTransaccion)
                    .IsRequired()
                    .HasMaxLength(4);

                entity.Property(e => e.MontoTransaccion).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.ObsvAnulacion).HasDefaultValueSql("('')");

                entity.Property(e => e.RmontoTransaccion)
                    .HasColumnName("RMontoTransaccion")
                    .HasColumnType("numeric(18, 2)");

                entity.Property(e => e.RtotalDetalleCobrado)
                    .HasColumnName("RTotalDetalleCobrado")
                    .HasColumnType("numeric(18, 2)");

                entity.Property(e => e.TotalDetalleCobrado).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.TransferidoSap).HasColumnName("TransferidoSAP");

                entity.Property(e => e.UsuarioAnula).HasMaxLength(40);

                entity.Property(e => e.UsuarioAprueba).HasMaxLength(40);

                entity.Property(e => e.UsuarioConfirma).HasMaxLength(40);

                entity.Property(e => e.UsuarioRegistro)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.UsuarioReversa).HasMaxLength(40);

                entity.HasOne(d => d.IdClienteNavigation)
                    .WithMany(p => p.CobGeneralCobranza)
                    .HasForeignKey(d => d.IdCliente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CobGeneralCobranza_MtrCliente");

                entity.HasOne(d => d.IdMtrTipoMonedaNavigation)
                    .WithMany(p => p.CobGeneralCobranza)
                    .HasForeignKey(d => d.IdMtrTipoMoneda)
                    .HasConstraintName("FK_CobGeneralCobranza_MtrTipoMoneda");

                entity.HasOne(d => d.IdTipoTransaccionNavigation)
                    .WithMany(p => p.CobGeneralCobranza)
                    .HasForeignKey(d => d.IdTipoTransaccion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CobGeneralCobranza_CobTipoTransaccion");
            });

            modelBuilder.Entity<CobGrabacionCobranzas>(entity =>
            {
                entity.HasIndex(e => e.Cotizacion)
                    .HasName("IX_CobGrabacionCobranzas_4");

                entity.HasIndex(e => e.DocAfecta)
                    .HasName("IX_CobGrabacionCobranzas_1");

                entity.HasIndex(e => e.Documento)
                    .HasName("IX_CobGrabacionCobranzas");

                entity.HasIndex(e => e.Id)
                    .HasName("IX_CobGrabacionCobranzas_2");

                entity.HasIndex(e => new { e.Transaccion, e.Cotizacion })
                    .HasName("IX_CobGrabacionCobranzas_3");

                entity.Property(e => e.Cotizacion)
                    .HasMaxLength(13)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DocAfecta).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.DocAfectaSap).HasMaxLength(10);

                entity.Property(e => e.Monto).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.Rmonto)
                    .HasColumnName("RMonto")
                    .HasColumnType("numeric(18, 2)");

                entity.Property(e => e.Rpdct)
                    .IsRequired()
                    .HasColumnName("RPDCT")
                    .HasMaxLength(2)
                    .IsFixedLength()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Rpdoc).HasColumnName("RPDOC");

                entity.Property(e => e.Rpkco)
                    .IsRequired()
                    .HasColumnName("RPKCO")
                    .HasMaxLength(5)
                    .IsFixedLength()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Rpsfx)
                    .IsRequired()
                    .HasColumnName("RPSFX")
                    .HasMaxLength(3)
                    .IsFixedLength()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Transaccion).HasMaxLength(2);

                entity.HasOne(d => d.DocumentoNavigation)
                    .WithMany(p => p.CobGrabacionCobranzas)
                    .HasForeignKey(d => d.Documento)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_CobGrabacionCobranzas_CobGeneralCobranza");

                entity.HasOne(d => d.TransaccionNavigation)
                    .WithMany(p => p.CobGrabacionCobranzas)
                    .HasPrincipalKey(p => p.Transaccion)
                    .HasForeignKey(d => d.Transaccion)
                    .HasConstraintName("FK_CobGrabacionCobranzas_CobTransacciones");
            });

            modelBuilder.Entity<CobIntegridadCobranzasVsWary074>(entity =>
            {
                entity.HasKey(e => e.Documento);

                entity.Property(e => e.Documento).ValueGeneratedNever();

                entity.Property(e => e.Cliente).HasMaxLength(10);

                entity.Property(e => e.Diferencia).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.FechaEvaluacion).HasColumnType("datetime");

                entity.Property(e => e.FechaRegistro).HasColumnType("smalldatetime");

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.MontoCobrado).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.MontoRetenido).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.TotalCobradoWary074).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.TotalCobroRetencion).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.TotalSapCobranzas).HasColumnType("numeric(18, 2)");
            });

            modelBuilder.Entity<CobPagosRetenciones>(entity =>
            {
                entity.HasIndex(e => new { e.IdCobranza, e.IdTransaccion, e.Porcentaje })
                    .HasName("IX_CobPagosRetenciones")
                    .IsUnique();

                entity.Property(e => e.FechaComprobante)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FechaCrea).HasColumnType("datetime");

                entity.Property(e => e.Monto).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.NroComprobante)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Porcentaje).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.Rmonto)
                    .HasColumnName("RMonto")
                    .HasColumnType("numeric(18, 2)");

                entity.HasOne(d => d.IdCobranzaNavigation)
                    .WithMany(p => p.CobPagosRetenciones)
                    .HasForeignKey(d => d.IdCobranza)
                    .HasConstraintName("FK_CobPagosRetenciones_CobGrabacionCobranzas");
            });

            modelBuilder.Entity<CobPorcentajesImpuestos>(entity =>
            {
                entity.HasKey(e => e.IdPorcImpuestos);

                entity.Property(e => e.Desde).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.FechaCreacion).HasColumnType("smalldatetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("smalldatetime");

                entity.Property(e => e.Hasta).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.Incremento).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.UsuarioCreacion).HasMaxLength(50);

                entity.Property(e => e.UsuarioModificacion).HasMaxLength(50);
            });

            modelBuilder.Entity<CobRangosVencimiento>(entity =>
            {
                entity.HasKey(e => e.Codigo);

                entity.ToTable("Cob_Rangos_Vencimiento");

                entity.Property(e => e.Codigo).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Descripcion).HasMaxLength(50);

                entity.Property(e => e.DiasDesde).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.DiasHasta).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.PieDos).HasColumnName("Pie_Dos");

                entity.Property(e => e.PieUno).HasColumnName("Pie_Uno");
            });

            modelBuilder.Entity<CobResumenEstadoCuenta>(entity =>
            {
                entity.HasKey(e => e.Cliente);

                entity.ToTable("Cob_Resumen_Estado_Cuenta");

                entity.HasIndex(e => e.Consultor)
                    .HasName("IX_Cob_Resumen_Estado_Cuenta_1");

                entity.HasIndex(e => e.Oficina)
                    .HasName("IX_Cob_Resumen_Estado_Cuenta");

                entity.Property(e => e.Cliente).HasMaxLength(10);

                entity.Property(e => e.Consultor).HasMaxLength(4);

                entity.Property(e => e.DescripcionRango).HasMaxLength(80);

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.NombreCliente).HasMaxLength(80);

                entity.Property(e => e.NombreConsultor).HasMaxLength(80);

                entity.Property(e => e.NombreOficina).HasMaxLength(80);

                entity.Property(e => e.Saldo).HasColumnType("numeric(18, 2)");
            });

            modelBuilder.Entity<CobResumenEstadoCuentaTemp>(entity =>
            {
                entity.HasKey(e => e.Cliente);

                entity.ToTable("Cob_Resumen_Estado_Cuenta_Temp");

                entity.Property(e => e.Cliente).HasMaxLength(10);

                entity.Property(e => e.Consultor).HasMaxLength(4);

                entity.Property(e => e.DescripcionRango).HasMaxLength(80);

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.NombreCliente).HasMaxLength(80);

                entity.Property(e => e.NombreConsultor).HasMaxLength(80);

                entity.Property(e => e.NombreOficina).HasMaxLength(80);

                entity.Property(e => e.Saldo).HasColumnType("numeric(18, 2)");
            });

            modelBuilder.Entity<CobRolCobranza>(entity =>
            {
                entity.HasKey(e => e.IdRolCobranza);

                entity.HasIndex(e => new { e.IdUsuario, e.IdOficina })
                    .HasName("IX_CobRolCobranza")
                    .IsUnique();

                entity.Property(e => e.FechaRegistro)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IdUsuario)
                    .IsRequired()
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<CobSysfile>(entity =>
            {
                entity.Property(e => e.FechaLm)
                    .HasColumnName("FechaLM")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaLmcxC)
                    .HasColumnName("FechaLMCxC")
                    .HasColumnType("datetime");

                entity.Property(e => e.FlagAprobarRc)
                    .IsRequired()
                    .HasColumnName("FlagAprobarRC")
                    .HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<CobTipoTransaccion>(entity =>
            {
                entity.HasKey(e => e.IdTipoTransaccion);

                entity.Property(e => e.IdTipoTransaccion).HasMaxLength(4);

                entity.Property(e => e.ColetillaIva)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IdTipoPagoSap)
                    .IsRequired()
                    .HasColumnName("IdTipoPagoSAP")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.NombreTipoTransaccion)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<CobTipoTransaccionRespaldoAntesEliminarColettilla>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.ColetillaIva)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.IdTipoPagoSap)
                    .IsRequired()
                    .HasColumnName("IdTipoPagoSAP")
                    .HasMaxLength(50);

                entity.Property(e => e.IdTipoTransaccion)
                    .IsRequired()
                    .HasMaxLength(4);

                entity.Property(e => e.NombreTipoTransaccion)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<CobTransacciones>(entity =>
            {
                entity.HasKey(e => e.IdTransacccionCobranzas);

                entity.HasIndex(e => e.TransLegacy)
                    .HasName("IX_CobTransacciones_1")
                    .IsUnique();

                entity.HasIndex(e => e.Transaccion)
                    .HasName("IX_CobTransacciones")
                    .IsUnique();

                entity.Property(e => e.BsHolgura).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.CuentaContable)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CuentaSap)
                    .IsRequired()
                    .HasColumnName("CuentaSAP")
                    .HasMaxLength(10);

                entity.Property(e => e.FechaCreacion).HasColumnType("smalldatetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("smalldatetime");

                entity.Property(e => e.NombreTransaccion)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.TipoSap)
                    .IsRequired()
                    .HasColumnName("TipoSAP")
                    .HasMaxLength(20);

                entity.Property(e => e.TransLegacy)
                    .IsRequired()
                    .HasMaxLength(2);

                entity.Property(e => e.Transaccion)
                    .IsRequired()
                    .HasMaxLength(2);

                entity.Property(e => e.UsuarioCreacion).HasMaxLength(40);

                entity.Property(e => e.UsuarioModificacion).HasMaxLength(40);
            });

            modelBuilder.Entity<CobValoresRetencionIva>(entity =>
            {
                entity.HasKey(e => e.IdRetIva);

                entity.Property(e => e.FechaCreacion).HasColumnType("smalldatetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("smalldatetime");

                entity.Property(e => e.PorcRetIva).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.UsuarioCreacion).HasMaxLength(40);

                entity.Property(e => e.UsuarioModificacion).HasMaxLength(40);
            });

            modelBuilder.Entity<CobValoresRetenciones>(entity =>
            {
                entity.HasKey(e => e.IdTransaccion);

                entity.Property(e => e.IdTransaccion).ValueGeneratedNever();

                entity.Property(e => e.FechaModifica).HasColumnType("datetime");

                entity.Property(e => e.FechaRegistro)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UsuarioModifica).HasMaxLength(40);

                entity.Property(e => e.UsuarioRegistro)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.Valores)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .HasName("Company_Name_Index")
                    .IsUnique();

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DataInicial>(entity =>
            {
                entity.HasIndex(e => e.Archivo)
                    .HasName("IX_DataInicial");

                entity.HasIndex(e => e.Data)
                    .HasName("index_Data_DataInicial");

                entity.HasIndex(e => e.Lote)
                    .HasName("IX_DataInicial_1");

                entity.HasIndex(e => e.OrdenArchivo)
                    .HasName("index_DataInicial_OrdenArchivo");

                entity.HasIndex(e => new { e.OrdenArchivo, e.Id })
                    .HasName("index_DataInicial_OrdenArchivoId");

                entity.Property(e => e.Id).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Archivo).HasMaxLength(100);

                entity.Property(e => e.Data).HasMaxLength(250);

                entity.Property(e => e.Lote).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.OrdenArchivo).HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<DeudorTipoRetencion>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Cliente).HasMaxLength(10);

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IndicadorRetencion).HasMaxLength(2);

                entity.Property(e => e.Nombre).HasMaxLength(255);

                entity.Property(e => e.TipoRetencion).HasMaxLength(2);
            });

            modelBuilder.Entity<Deudores>(entity =>
            {
                entity.HasKey(e => e.Cliente);

                entity.Property(e => e.Cliente).HasMaxLength(10);

                entity.Property(e => e.Clase).HasMaxLength(255);

                entity.Property(e => e.Nombre).HasMaxLength(255);

                entity.Property(e => e.Rif).HasMaxLength(255);
            });

         
         

            modelBuilder.Entity<MtrBancos>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("WARY075_INDEX01");

                entity.Property(e => e.Codigo)
                    .HasColumnName("CODIGO")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.AplicaImp)
                    .IsRequired()
                    .HasColumnName("APLICA_IMP")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CodContable)
                    .IsRequired()
                    .HasColumnName("COD_CONTABLE")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.ProdGenerico)
                    .HasColumnName("PROD_GENERICO")
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.Recnum)
                    .HasColumnName("RECNUM")
                    .HasColumnType("decimal(28, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.TraAsociada)
                    .HasColumnName("TRA_ASOCIADA")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.TraJde)
                    .HasColumnName("TRA_JDE")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.IdTipoCuentaDestinoNavigation)
                    .WithMany(p => p.MtrBancos)
                    .HasForeignKey(d => d.IdTipoCuentaDestino)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MtrBancos_MtrTipoCuentaDestino");
            });

            modelBuilder.Entity<MtrCliente>(entity =>
            {
                entity.HasKey(e => e.Codigo);

                entity.HasIndex(e => e.CodJde)
                    .HasName("IX_MtrCliente_2");

                entity.HasIndex(e => e.Codigo)
                    .HasName("IX_MtrCliente");

                entity.HasIndex(e => e.Nombre)
                    .HasName("IX_MtrCliente_4");

                entity.HasIndex(e => e.OficinaVenta)
                    .HasName("IX_MtrCliente_3");

                entity.HasIndex(e => e.Vendedor1)
                    .HasName("IX_MtrCliente_1");

                entity.Property(e => e.Codigo).HasMaxLength(10);

                entity.Property(e => e.AreaCobranz)
                    .HasColumnName("Area_Cobranz")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Asignacion)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Canal)
                    .HasColumnName("CANAL")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CapitalSoc)
                    .HasColumnName("CAPITAL_SOC")
                    .HasColumnType("numeric(18, 2)");

                entity.Property(e => e.Categoria)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CierreEjerc)
                    .HasColumnName("CIERRE_EJERC")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ClientContado)
                    .HasColumnName("Client_Contado")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ClienteNuevo)
                    .HasColumnName("Cliente_Nuevo")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Cobrador)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodJde).HasColumnName("cod_jde");

                entity.Property(e => e.CodJdePadre).HasColumnName("Cod_jdePadre");

                entity.Property(e => e.CodMcpo)
                    .HasColumnName("Cod_Mcpo")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodProveedor)
                    .HasColumnName("Cod_Proveedor")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodSubSegmento)
                    .HasColumnName("Cod_SubSegmento")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodigoCiudad)
                    .HasColumnName("Codigo_Ciudad")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodigoEstado)
                    .HasColumnName("Codigo_Estado")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodigoMcpo)
                    .HasColumnName("Codigo_Mcpo")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodigoNumerico)
                    .HasColumnName("codigo_numerico")
                    .HasColumnType("numeric(6, 0)");

                entity.Property(e => e.CodigoParroq)
                    .HasColumnName("Codigo_Parroq")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodigoPostal)
                    .HasColumnName("Codigo_Postal")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodigoRegion)
                    .HasColumnName("Codigo_Region")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CondPago)
                    .HasColumnName("COND_PAGO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ContCarg1)
                    .HasColumnName("Cont_Carg_1")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContCarg2)
                    .HasColumnName("Cont_Carg_2")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContCarg3)
                    .HasColumnName("Cont_Carg_3")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContEmail1)
                    .HasColumnName("Cont_Email_1")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContEmail2)
                    .HasColumnName("Cont_Email_2")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContEmail3)
                    .HasColumnName("Cont_Email_3")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContEspecial)
                    .HasColumnName("Cont_Especial")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContFechNacm1)
                    .HasColumnName("Cont_FechNacm_1")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ContFechNacm2)
                    .HasColumnName("Cont_FechNacm_2")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ContFechNacm3)
                    .HasColumnName("Cont_FechNacm_3")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ContNomb1)
                    .HasColumnName("Cont_Nomb_1")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContNomb2)
                    .HasColumnName("Cont_Nomb_2")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContNomb3)
                    .HasColumnName("Cont_Nomb_3")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContTelf1)
                    .HasColumnName("Cont_Telf_1")
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContTelf2)
                    .HasColumnName("Cont_Telf_2")
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContTelf3)
                    .HasColumnName("Cont_Telf_3")
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.DepuraCod)
                    .HasColumnName("DEPURA_COD")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Depurar)
                    .HasColumnName("depurar")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.DiaDeCobro)
                    .HasColumnName("Dia_de_Cobro")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Direccion).HasMaxLength(80);

                entity.Property(e => e.Direccion1).HasMaxLength(80);

                entity.Property(e => e.Direccion2).HasMaxLength(80);

                entity.Property(e => e.EMailClient)
                    .HasColumnName("E_mail_Client")
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Estado).HasMaxLength(2);

                entity.Property(e => e.ExentoDeImpuesto)
                    .HasColumnName("Exento_de_Impuesto")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FApertura)
                    .HasColumnName("F_Apertura")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FModificacion)
                    .HasColumnName("F_Modificacion")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FUltmCompra)
                    .HasColumnName("F_Ultm_Compra")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Fax)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FechaAtendido)
                    .HasColumnName("Fecha_Atendido")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FechaCerrado)
                    .HasColumnName("Fecha_Cerrado")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FechaDesatendido)
                    .HasColumnName("Fecha_Desatendido")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FechaInactivo)
                    .HasColumnName("Fecha_Inactivo")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FechaLegal)
                    .HasColumnName("FECHA_LEGAL")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FechaModificadoLimite)
                    .HasColumnName("Fecha_modificado_Limite")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FechaReactivado)
                    .HasColumnName("Fecha_Reactivado")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FechaRiesgo)
                    .HasColumnName("FECHA_RIESGO")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FlagAtendido)
                    .HasColumnName("Flag_Atendido")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.FlagCerrado)
                    .HasColumnName("Flag_Cerrado")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FlagInactivo)
                    .HasColumnName("Flag_Inactivo")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FlagMat)
                    .HasColumnName("Flag_Mat")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FlagMod03)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FlagModificado)
                    .HasColumnName("Flag_Modificado")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FlagNuevo)
                    .HasColumnName("Flag_Nuevo")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.GiroNegocio)
                    .HasColumnName("Giro_Negocio")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Grupo)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.GrupoCliente)
                    .HasColumnName("GRUPO_CLIENTE")
                    .HasColumnType("numeric(5, 0)");

                entity.Property(e => e.IdDireccion)
                    .HasColumnName("Id_direccion")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Klabc)
                    .HasColumnName("KLABC")
                    .HasMaxLength(2)
                    .HasDefaultValueSql("(N'C')");

                entity.Property(e => e.LCredito)
                    .HasColumnName("L_Credito")
                    .HasColumnType("money");

                entity.Property(e => e.Legal)
                    .HasColumnName("LEGAL")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LimiteCredUt)
                    .HasColumnName("Limite_Cred_Ut")
                    .HasColumnType("numeric(16, 2)");

                entity.Property(e => e.Municipio).HasMaxLength(2);

                entity.Property(e => e.Nit)
                    .HasMaxLength(14)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.NoPasoJde)
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.NoRegTribut)
                    .HasColumnName("No_Reg_Tribut")
                    .HasMaxLength(12);

                entity.Property(e => e.Nombre).HasMaxLength(80);

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(70)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.OficinaVenta)
                    .HasColumnName("Oficina_Venta")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.OrdenCompra)
                    .HasColumnName("Orden_Compra")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.PaginaWeb)
                    .HasColumnName("Pagina_Web")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.RepresLegal)
                    .HasColumnName("Repres_Legal")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Riesgo)
                    .HasColumnName("RIESGO")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.RutCartero)
                    .HasColumnName("Rut_Cartero")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Saldo).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.Segmento)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.SiDolares)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Smacc)
                    .HasColumnName("smacc")
                    .HasMaxLength(50);

                entity.Property(e => e.SubSegmentoa)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TImpuesto).HasColumnName("T_Impuesto");

                entity.Property(e => e.Telefono)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Telefono1)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TipoNegocio)
                    .HasColumnName("Tipo_Negocio")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.UsuarioMod)
                    .HasColumnName("USUARIO_MOD")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Vendedor1)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Zona)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Zzsn01)
                    .HasColumnName("ZZSN01")
                    .HasMaxLength(50)
                    .HasComment("Página Web");

                entity.Property(e => e.Zzsn02)
                    .HasColumnName("ZZSN02")
                    .HasMaxLength(50)
                    .HasComment("Facebook");

                entity.Property(e => e.Zzsn03)
                    .HasColumnName("ZZSN03")
                    .HasMaxLength(50)
                    .HasComment("Google+");

                entity.Property(e => e.Zzsn04)
                    .HasColumnName("ZZSN04")
                    .HasMaxLength(50)
                    .HasComment("Instagram");

                entity.Property(e => e.Zzsn05)
                    .HasColumnName("ZZSN05")
                    .HasMaxLength(50)
                    .HasComment("Twitter");

                entity.Property(e => e.Zzsn06)
                    .HasColumnName("ZZSN06")
                    .HasMaxLength(8);
            });

            modelBuilder.Entity<MtrClienteProspecto>(entity =>
            {
                entity.HasKey(e => e.IdCliente);

                entity.HasIndex(e => e.Rif)
                    .HasName("IX_Rif")
                    .IsUnique();

                entity.Property(e => e.IdCliente).HasMaxLength(10);

                entity.Property(e => e.Direccion).HasMaxLength(240);

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IdVendedor).HasMaxLength(10);

                entity.Property(e => e.NombreCliente).HasMaxLength(80);

                entity.Property(e => e.Rif).HasMaxLength(12);
            });

            modelBuilder.Entity<MtrClienteTemp>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("PK_MtrCliente_temp");

                entity.ToTable("MtrCliente_Temp");

                entity.Property(e => e.Codigo).HasMaxLength(10);

                entity.Property(e => e.AreaCobranz)
                    .HasColumnName("Area_Cobranz")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Asignacion)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Canal)
                    .HasColumnName("CANAL")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CapitalSoc)
                    .HasColumnName("CAPITAL_SOC")
                    .HasColumnType("numeric(18, 2)");

                entity.Property(e => e.Categoria)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CierreEjerc)
                    .HasColumnName("CIERRE_EJERC")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ClientContado)
                    .HasColumnName("Client_Contado")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ClienteNuevo)
                    .HasColumnName("Cliente_Nuevo")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Cobrador)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodJde).HasColumnName("cod_jde");

                entity.Property(e => e.CodJdePadre).HasColumnName("Cod_jdePadre");

                entity.Property(e => e.CodMcpo)
                    .HasColumnName("Cod_Mcpo")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodProveedor)
                    .HasColumnName("Cod_Proveedor")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodSubSegmento)
                    .HasColumnName("Cod_SubSegmento")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodigoCiudad)
                    .HasColumnName("Codigo_Ciudad")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodigoEstado)
                    .HasColumnName("Codigo_Estado")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodigoMcpo)
                    .HasColumnName("Codigo_Mcpo")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodigoNumerico)
                    .HasColumnName("codigo_numerico")
                    .HasColumnType("numeric(6, 0)");

                entity.Property(e => e.CodigoParroq)
                    .HasColumnName("Codigo_Parroq")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodigoPostal)
                    .HasColumnName("Codigo_Postal")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodigoRegion)
                    .HasColumnName("Codigo_Region")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CondPago)
                    .HasColumnName("COND_PAGO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ContCarg1)
                    .HasColumnName("Cont_Carg_1")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContCarg2)
                    .HasColumnName("Cont_Carg_2")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContCarg3)
                    .HasColumnName("Cont_Carg_3")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContEmail1)
                    .HasColumnName("Cont_Email_1")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContEmail2)
                    .HasColumnName("Cont_Email_2")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContEmail3)
                    .HasColumnName("Cont_Email_3")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContEspecial)
                    .HasColumnName("Cont_Especial")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContFechNacm1)
                    .HasColumnName("Cont_FechNacm_1")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ContFechNacm2)
                    .HasColumnName("Cont_FechNacm_2")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ContFechNacm3)
                    .HasColumnName("Cont_FechNacm_3")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.ContNomb1)
                    .HasColumnName("Cont_Nomb_1")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContNomb2)
                    .HasColumnName("Cont_Nomb_2")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContNomb3)
                    .HasColumnName("Cont_Nomb_3")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContTelf1)
                    .HasColumnName("Cont_Telf_1")
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContTelf2)
                    .HasColumnName("Cont_Telf_2")
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContTelf3)
                    .HasColumnName("Cont_Telf_3")
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.DepuraCod)
                    .HasColumnName("DEPURA_COD")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Depurar)
                    .HasColumnName("depurar")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.DiaDeCobro)
                    .HasColumnName("Dia_de_Cobro")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Direccion).HasMaxLength(80);

                entity.Property(e => e.Direccion1).HasMaxLength(80);

                entity.Property(e => e.Direccion2).HasMaxLength(80);

                entity.Property(e => e.EMailClient)
                    .HasColumnName("E_mail_Client")
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Estado).HasMaxLength(2);

                entity.Property(e => e.ExentoDeImpuesto)
                    .HasColumnName("Exento_de_Impuesto")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FApertura)
                    .HasColumnName("F_Apertura")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FModificacion)
                    .HasColumnName("F_Modificacion")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FUltmCompra)
                    .HasColumnName("F_Ultm_Compra")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Fax)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FechaAtendido)
                    .HasColumnName("Fecha_Atendido")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FechaCerrado)
                    .HasColumnName("Fecha_Cerrado")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FechaDesatendido)
                    .HasColumnName("Fecha_Desatendido")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FechaInactivo)
                    .HasColumnName("Fecha_Inactivo")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FechaLegal)
                    .HasColumnName("FECHA_LEGAL")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FechaModificadoLimite)
                    .HasColumnName("Fecha_modificado_Limite")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FechaReactivado)
                    .HasColumnName("Fecha_Reactivado")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FechaRiesgo)
                    .HasColumnName("FECHA_RIESGO")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FlagAtendido)
                    .HasColumnName("Flag_Atendido")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.FlagCerrado)
                    .HasColumnName("Flag_Cerrado")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FlagInactivo)
                    .HasColumnName("Flag_Inactivo")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FlagMat)
                    .HasColumnName("Flag_Mat")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FlagMod03)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FlagModificado)
                    .HasColumnName("Flag_Modificado")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FlagNuevo)
                    .HasColumnName("Flag_Nuevo")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.GiroNegocio)
                    .HasColumnName("Giro_Negocio")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Grupo)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.GrupoCliente)
                    .HasColumnName("GRUPO_CLIENTE")
                    .HasColumnType("numeric(5, 0)");

                entity.Property(e => e.IdDireccion)
                    .HasColumnName("Id_direccion")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Klabc)
                    .HasColumnName("KLABC")
                    .HasMaxLength(2)
                    .HasDefaultValueSql("(N'C')");

                entity.Property(e => e.LCredito)
                    .HasColumnName("L_Credito")
                    .HasColumnType("money");

                entity.Property(e => e.Legal)
                    .HasColumnName("LEGAL")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LimiteCredUt)
                    .HasColumnName("Limite_Cred_Ut")
                    .HasColumnType("numeric(16, 2)");

                entity.Property(e => e.Municipio).HasMaxLength(2);

                entity.Property(e => e.Nit)
                    .HasMaxLength(14)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.NoPasoJde)
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.NoRegTribut)
                    .HasColumnName("No_Reg_Tribut")
                    .HasMaxLength(12);

                entity.Property(e => e.Nombre).HasMaxLength(80);

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(70)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.OficinaVenta)
                    .HasColumnName("Oficina_Venta")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.OrdenCompra)
                    .HasColumnName("Orden_Compra")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.PaginaWeb)
                    .HasColumnName("Pagina_Web")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.RepresLegal)
                    .HasColumnName("Repres_Legal")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Riesgo)
                    .HasColumnName("RIESGO")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.RutCartero)
                    .HasColumnName("Rut_Cartero")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Saldo).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.Segmento)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.SiDolares)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Smacc)
                    .HasColumnName("smacc")
                    .HasMaxLength(50);

                entity.Property(e => e.SubSegmentoa)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TImpuesto).HasColumnName("T_Impuesto");

                entity.Property(e => e.Telefono)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Telefono1)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TipoNegocio)
                    .HasColumnName("Tipo_Negocio")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.UsuarioMod)
                    .HasColumnName("USUARIO_MOD")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Vendedor1)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Zona)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Zzsn01)
                    .HasColumnName("ZZSN01")
                    .HasMaxLength(50);

                entity.Property(e => e.Zzsn02)
                    .HasColumnName("ZZSN02")
                    .HasMaxLength(50);

                entity.Property(e => e.Zzsn03)
                    .HasColumnName("ZZSN03")
                    .HasMaxLength(50);

                entity.Property(e => e.Zzsn04)
                    .HasColumnName("ZZSN04")
                    .HasMaxLength(50);

                entity.Property(e => e.Zzsn05)
                    .HasColumnName("ZZSN05")
                    .HasMaxLength(50);

                entity.Property(e => e.Zzsn06)
                    .HasColumnName("ZZSN06")
                    .HasMaxLength(8);
            });

            modelBuilder.Entity<MtrCondicionPago>(entity =>
            {
                entity.HasKey(e => e.Codigo);

                entity.Property(e => e.Codigo)
                    .HasColumnName("CODIGO")
                    .ValueGeneratedNever();

                entity.Property(e => e.AprobCredM)
                    .HasColumnName("APROB_CRED_M")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodJde)
                    .HasColumnName("COD_JDE")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.CondicionSap)
                    .IsRequired()
                    .HasColumnName("CondicionSAP")
                    .HasMaxLength(4)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DiasVcto).HasColumnName("DIAS_VCTO");

                entity.Property(e => e.Inactivo)
                    .HasColumnName("inactivo")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.PorcRequerido).HasColumnType("numeric(18, 4)");

                entity.Property(e => e.Recnum)
                    .HasColumnName("RECNUM")
                    .HasColumnType("decimal(28, 0)");

                entity.Property(e => e.RequiereLimiteCredito)
                    .HasMaxLength(1)
                    .IsFixedLength();
            });

            modelBuilder.Entity<MtrContactos>(entity =>
            {
                entity.HasKey(e => e.IdContacto);

                entity.HasIndex(e => e.Email)
                    .HasName("IX_MtrContactos");

                entity.HasIndex(e => e.IdCliente)
                    .HasName("IX_MtrContactos_1");

                entity.HasIndex(e => new { e.IdCliente, e.Rif })
                    .HasName("IX_MtrContactos_2");

                entity.Property(e => e.IdContacto).ValueGeneratedNever();

                entity.Property(e => e.AceptaEmail).HasDefaultValueSql("((0))");

                entity.Property(e => e.AceptaSms).HasDefaultValueSql("((0))");

                entity.Property(e => e.Area)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Cargo)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Celular)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Fax)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IdCliente)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Inactivo).HasDefaultValueSql("((0))");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Prospecto).HasDefaultValueSql("((0))");

                entity.Property(e => e.Rif)
                    .IsRequired()
                    .HasMaxLength(12);

                entity.Property(e => e.Telefono1)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Telefono2)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Usuario).HasMaxLength(50);
            });

            modelBuilder.Entity<MtrEmpleado>(entity =>
            {
                entity.HasKey(e => e.IdEmpleado);

                entity.Property(e => e.IdEmpleado)
                    .HasMaxLength(10)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Categoria1)
                    .HasMaxLength(4)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Categoria2)
                    .HasMaxLength(4)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Categoria3)
                    .HasMaxLength(4)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Cedula)
                    .HasMaxLength(20)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Cuenta)
                    .HasMaxLength(20)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Direccion1)
                    .HasMaxLength(40)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Direccion2)
                    .HasMaxLength(40)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(80)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FechaIngreso).HasColumnType("datetime");

                entity.Property(e => e.FechaNacimiento).HasColumnType("datetime");

                entity.Property(e => e.FechaRetiro).HasColumnType("datetime");

                entity.Property(e => e.IdCargo)
                    .HasMaxLength(10)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IdDepartamento)
                    .HasMaxLength(10)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IdGrupo)
                    .HasMaxLength(2)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IdSupervisor)
                    .HasMaxLength(10)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.NombreEmpleado)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Rif)
                    .HasMaxLength(15)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.SexoEmpleado)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TelefonoCelular)
                    .HasMaxLength(15)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TelefonoHabitacion)
                    .HasMaxLength(15)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TipoNomina)
                    .IsRequired()
                    .HasMaxLength(4)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<MtrFechaFeriada>(entity =>
            {
                entity.HasKey(e => e.FechaFeriada);

                entity.Property(e => e.FechaFeriada)
                    .HasColumnName("FECHA_FERIADA")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('01/01/1753')");

                entity.Property(e => e.MostrarBs)
                    .IsRequired()
                    .HasColumnName("MOSTRAR_BS")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.Motivo)
                    .IsRequired()
                    .HasColumnName("MOTIVO")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.Recnum)
                    .HasColumnName("RECNUM")
                    .HasColumnType("decimal(28, 0)")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<MtrHorarioLaborable>(entity =>
            {
                entity.HasKey(e => e.IdHorario);

                entity.Property(e => e.Almuerzo)
                    .IsRequired()
                    .HasMaxLength(1);

                entity.Property(e => e.Fin)
                    .HasColumnName("fin")
                    .HasColumnType("datetime");

                entity.Property(e => e.Inicio).HasColumnType("datetime");

                entity.Property(e => e.Laborable)
                    .IsRequired()
                    .HasMaxLength(1);

                entity.Property(e => e.PrimerBloque)
                    .IsRequired()
                    .HasMaxLength(1);
            });

            modelBuilder.Entity<MtrOficina>(entity =>
            {
                entity.HasKey(e => e.CodOficina);

                entity.Property(e => e.CodOficina)
                    .HasColumnName("COD_OFICINA")
                    .ValueGeneratedNever();

                entity.Property(e => e.Abreviado).HasMaxLength(3);

                entity.Property(e => e.AdministradoraEmail)
                    .HasColumnName("Administradora_email")
                    .HasMaxLength(50);

                entity.Property(e => e.Analista2Email)
                    .HasColumnName("Analista2_email")
                    .HasMaxLength(50);

                entity.Property(e => e.AnalistaEmail)
                    .HasColumnName("Analista_email")
                    .HasMaxLength(50);

                entity.Property(e => e.Canal)
                    .HasColumnName("CANAL")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CarpetaDiseno)
                    .HasColumnName("CARPETA_DISENO")
                    .HasMaxLength(50);

                entity.Property(e => e.ClienteNacionales)
                    .HasColumnName("CLIENTE_NACIONALES")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodOficina2)
                    .IsRequired()
                    .HasColumnName("COD_OFICINA2")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoJde)
                    .HasColumnName("CODIGO_JDE")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoParroq).HasColumnName("CODIGO_PARROQ");

                entity.Property(e => e.DataManagerEmail)
                    .HasColumnName("DataManager_email")
                    .HasMaxLength(50);

                entity.Property(e => e.Direccion)
                    .IsRequired()
                    .HasColumnName("DIRECCION")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion2)
                    .IsRequired()
                    .HasColumnName("DIRECCION2")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("ESTADO")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .IsRequired()
                    .HasColumnName("FAX")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.GerenteOficina)
                    .IsRequired()
                    .HasColumnName("GERENTE_OFICINA")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Idtitulo).HasColumnName("IDTITULO");

                entity.Property(e => e.Municipio)
                    .IsRequired()
                    .HasColumnName("MUNICIPIO")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.NomOficina)
                    .IsRequired()
                    .HasColumnName("NOM_OFICINA")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.OficinaActiva)
                    .HasColumnName("OFICINA_ACTIVA")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Override)
                    .HasColumnName("OVERRIDE")
                    .HasColumnType("decimal(16, 2)");

                entity.Property(e => e.PAusente)
                    .HasColumnName("P_AUSENTE")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Recnum)
                    .HasColumnName("RECNUM")
                    .HasColumnType("decimal(28, 0)");

                entity.Property(e => e.Region)
                    .IsRequired()
                    .HasColumnName("REGION")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SinImpuesto)
                    .HasColumnName("SIN_IMPUESTO")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Telefono)
                    .IsRequired()
                    .HasColumnName("TELEFONO")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.UsuarioAdministradora).HasMaxLength(50);

                entity.Property(e => e.UsuarioAnalista).HasMaxLength(50);
            });

            modelBuilder.Entity<MtrPerfilCorreo>(entity =>
            {
                entity.HasKey(e => e.PerfilId);

                entity.Property(e => e.PerfilId).HasMaxLength(50);

                entity.Property(e => e.Id).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Perfil).HasMaxLength(50);
            });

            modelBuilder.Entity<MtrProducto>(entity =>
            {
                entity.HasKey(e => e.CodigoProduct)
                    .HasName("MtrProducto_INDEX01");

                entity.HasIndex(e => e.Recnum)
                    .HasName("IX_MtrProducto");

                entity.Property(e => e.CodigoProduct)
                    .HasColumnName("CODIGO_PRODUCT")
                    .HasMaxLength(12)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.Almacensap)
                    .HasColumnName("ALMACENSAP")
                    .HasMaxLength(4);

                entity.Property(e => e.CantidadXAnch).HasColumnName("CANTIDAD_X_ANCH");

                entity.Property(e => e.CategoriaDashBoard)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CodJde)
                    .HasColumnName("COD_JDE")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.CodResponsable)
                    .HasColumnName("COD_RESPONSABLE")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Codcorp)
                    .HasColumnName("CODCORP")
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CodigoCaja)
                    .IsRequired()
                    .HasMaxLength(6)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.CodprodRpg)
                    .HasColumnName("CODPROD_RPG")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Consecutivo)
                    .HasColumnName("consecutivo")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.CostoReposicion)
                    .HasColumnType("decimal(18, 2)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DescripVta)
                    .HasColumnName("DESCRIP_VTA")
                    .IsUnicode(false);

                entity.Property(e => e.DescripcionVentas).HasMaxLength(1000);

                entity.Property(e => e.FamiliaProduct)
                    .HasColumnName("FAMILIA_PRODUCT")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FlagCorp)
                    .HasColumnName("FLAG_CORP")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FlagCortoTiraje).HasMaxLength(1);

                entity.Property(e => e.FlagDesact)
                    .HasColumnName("FLAG_DESACT")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FlagPrdIntern)
                    .HasColumnName("FLAG_PRD_INTERN")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FlagPrecioMinimo)
                    .HasMaxLength(1)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FlagRollo)
                    .HasColumnName("FLAG_ROLLO")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IdUnidadCosteoSap)
                    .HasColumnName("IdUnidadCosteoSAP")
                    .HasMaxLength(18);

                entity.Property(e => e.Idsubcategoria)
                    .HasColumnName("IDSUBCATEGORIA")
                    .HasColumnType("decimal(4, 0)");

                entity.Property(e => e.ImpPorConsumo)
                    .HasColumnName("IMP_POR_CONSUMO")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.LineaIncoming).HasColumnName("LINEA_INCOMING");

                entity.Property(e => e.LineaProducto)
                    .HasColumnName("LINEA_PRODUCTO")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ManejaInv)
                    .HasColumnName("MANEJA_INV")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Miscelaneo)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(65)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.Numero).HasColumnName("NUMERO");

                entity.Property(e => e.PesoKilos).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.PiesCuadradoUnidad).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PorcCosto).HasColumnName("PORC_COSTO");

                entity.Property(e => e.PorcMcAprob)
                    .HasColumnName("PORC_MC_APROB")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Prefijo)
                    .HasColumnName("PREFIJO")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.PrintNoPrint)
                    .HasColumnName("Print_NoPrint")
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Recnum)
                    .HasColumnName("RECNUM")
                    .HasColumnType("decimal(28, 0)");

                entity.Property(e => e.Sufijo)
                    .HasColumnName("SUFIJO")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.SujetoALote)
                    .HasColumnName("SUJETO_A_LOTE")
                    .HasMaxLength(1);

                entity.Property(e => e.TipoProducto).HasColumnName("TIPO_PRODUCTO");

                entity.Property(e => e.Titulo)
                    .HasColumnName("TITULO")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Titulo1)
                    .HasColumnName("TITULO1")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Titulo2)
                    .HasColumnName("TITULO2")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Titulo3)
                    .HasColumnName("TITULO3")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Titulo4)
                    .HasColumnName("TITULO4")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Titulo5)
                    .HasColumnName("TITULO5")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Titulo6)
                    .HasColumnName("TITULO6")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TituloPe)
                    .HasColumnName("TITULO_PE")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TituloVentas)
                    .HasColumnName("TITULO_VENTAS")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.UnidadCosteo).HasColumnName("UNIDAD_COSTEO");

                entity.Property(e => e.Unidadsap)
                    .HasColumnName("UNIDADSAP")
                    .HasMaxLength(3);

                entity.Property(e => e.UnidadsapVenta)
                    .HasColumnName("UNIDADSAP_VENTA")
                    .HasMaxLength(3);

                entity.Property(e => e.UnidadsapVentainterna)
                    .HasColumnName("UNIDADSAP_VENTAINTERNA")
                    .HasMaxLength(3);

                entity.Property(e => e.Web)
                    .HasColumnName("WEB")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.IdUnidadCosteoSapNavigation)
                    .WithMany(p => p.MtrProducto)
                    .HasForeignKey(d => d.IdUnidadCosteoSap)
                    .HasConstraintName("FK_MtrProducto_T006A");
            });

            modelBuilder.Entity<MtrProductoBasica>(entity =>
            {
                entity.HasKey(e => new { e.Producto, e.MedidaMascara })
                    .HasName("PK_MtrProductoBasica_1");

                entity.Property(e => e.Producto).HasMaxLength(20);

                entity.Property(e => e.MedidaMascara).HasMaxLength(6);

                entity.Property(e => e.Frecuencia).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.MedidaFraccion)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<MtrProductoOpuesta>(entity =>
            {
                entity.HasKey(e => new { e.Producto, e.MedidaMascara })
                    .HasName("PK_MtrProductoOpuesta_1");

                entity.Property(e => e.Producto).HasMaxLength(20);

                entity.Property(e => e.MedidaMascara).HasMaxLength(6);

                entity.Property(e => e.Frecuencia).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.MedidaFraccion)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<MtrProductoPapeles>(entity =>
            {
                entity.HasKey(e => new { e.Producto, e.Parte, e.Papel });

                entity.Property(e => e.Producto).HasMaxLength(12);

                entity.Property(e => e.Papel).HasMaxLength(8);

                entity.Property(e => e.Descripcion).HasMaxLength(50);

                entity.Property(e => e.Frecuencia).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<MtrTipoCuentaDestino>(entity =>
            {
                entity.HasKey(e => e.IdTipoCuentaDestino);

                entity.Property(e => e.IdTipoCuentaDestino).ValueGeneratedNever();

                entity.Property(e => e.NombreCuentaDestino)
                    .IsRequired()
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<MtrTipoDocumentoFiscal>(entity =>
            {
                entity.HasKey(e => e.TipoDocumento);

                entity.Property(e => e.TipoDocumento).HasMaxLength(2);

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<MtrTipoMoneda>(entity =>
            {
                entity.Property(e => e.Descripcion).HasMaxLength(50);
            });

            modelBuilder.Entity<MtrTipoNomina>(entity =>
            {
                entity.HasKey(e => e.Tipnom)
                    .HasName("PK_NMT003");

                entity.Property(e => e.Tipnom)
                    .HasColumnName("TIPNOM")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Anocom)
                    .HasColumnName("ANOCOM")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Asifon)
                    .HasColumnName("ASIFON")
                    .HasColumnType("numeric(4, 0)");

                entity.Property(e => e.Canmin)
                    .HasColumnName("CANMIN")
                    .HasColumnType("numeric(2, 0)");

                entity.Property(e => e.CiaCodcia)
                    .HasColumnName("CIA_CODCIA")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Clanom)
                    .HasColumnName("CLANOM")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Codcon)
                    .HasColumnName("CODCON")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Codpai)
                    .HasColumnName("CODPAI")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Dedfon)
                    .HasColumnName("DEDFON")
                    .HasColumnType("numeric(4, 0)");

                entity.Property(e => e.Desm01)
                    .HasColumnName("DESM01")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm02)
                    .HasColumnName("DESM02")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm03)
                    .HasColumnName("DESM03")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm04)
                    .HasColumnName("DESM04")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm05)
                    .HasColumnName("DESM05")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm06)
                    .HasColumnName("DESM06")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm07)
                    .HasColumnName("DESM07")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm08)
                    .HasColumnName("DESM08")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm09)
                    .HasColumnName("DESM09")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm10)
                    .HasColumnName("DESM10")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm11)
                    .HasColumnName("DESM11")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm12)
                    .HasColumnName("DESM12")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm13)
                    .HasColumnName("DESM13")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm14)
                    .HasColumnName("DESM14")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm15)
                    .HasColumnName("DESM15")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm16)
                    .HasColumnName("DESM16")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm17)
                    .HasColumnName("DESM17")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm18)
                    .HasColumnName("DESM18")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm19)
                    .HasColumnName("DESM19")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desm20)
                    .HasColumnName("DESM20")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Desnom)
                    .HasColumnName("DESNOM")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Facgua)
                    .HasColumnName("FACGUA")
                    .HasColumnType("numeric(13, 10)");

                entity.Property(e => e.Fecabo)
                    .HasColumnName("FECABO")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fecact)
                    .HasColumnName("FECACT")
                    .HasColumnType("datetime");

                entity.Property(e => e.Feccre)
                    .HasColumnName("FECCRE")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fecto1)
                    .HasColumnName("FECTO1")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fecto2)
                    .HasColumnName("FECTO2")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fecto3)
                    .HasColumnName("FECTO3")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fecto4)
                    .HasColumnName("FECTO4")
                    .HasColumnType("datetime");

                entity.Property(e => e.Fecto5)
                    .HasColumnName("FECTO5")
                    .HasColumnType("datetime");

                entity.Property(e => e.Frenom)
                    .HasColumnName("FRENOM")
                    .HasColumnType("numeric(2, 0)");

                entity.Property(e => e.Fresue)
                    .HasColumnName("FRESUE")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Pgmrec)
                    .HasColumnName("PGMREC")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Reghab)
                    .HasColumnName("REGHAB")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Salmin)
                    .HasColumnName("SALMIN")
                    .HasColumnType("numeric(12, 5)");

                entity.Property(e => e.Tasmon)
                    .HasColumnName("TASMON")
                    .HasColumnType("numeric(13, 10)");

                entity.Property(e => e.Tipfec)
                    .HasColumnName("TIPFEC")
                    .HasColumnType("numeric(1, 0)");

                entity.Property(e => e.Tipmon)
                    .HasColumnName("TIPMON")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Topgua)
                    .HasColumnName("TOPGUA")
                    .HasColumnType("numeric(12, 5)");

                entity.Property(e => e.Unim01)
                    .HasColumnName("UNIM01")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim02)
                    .HasColumnName("UNIM02")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim03)
                    .HasColumnName("UNIM03")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim04)
                    .HasColumnName("UNIM04")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim05)
                    .HasColumnName("UNIM05")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim06)
                    .HasColumnName("UNIM06")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim07)
                    .HasColumnName("UNIM07")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim08)
                    .HasColumnName("UNIM08")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim09)
                    .HasColumnName("UNIM09")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim10)
                    .HasColumnName("UNIM10")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim11)
                    .HasColumnName("UNIM11")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim12)
                    .HasColumnName("UNIM12")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim13)
                    .HasColumnName("UNIM13")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim14)
                    .HasColumnName("UNIM14")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim15)
                    .HasColumnName("UNIM15")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim16)
                    .HasColumnName("UNIM16")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim17)
                    .HasColumnName("UNIM17")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim18)
                    .HasColumnName("UNIM18")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim19)
                    .HasColumnName("UNIM19")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Unim20)
                    .HasColumnName("UNIM20")
                    .HasColumnType("numeric(14, 2)");

                entity.Property(e => e.Usract)
                    .HasColumnName("USRACT")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Usrcre)
                    .HasColumnName("USRCRE")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Valfon)
                    .HasColumnName("VALFON")
                    .HasColumnType("numeric(7, 2)");

                entity.Property(e => e.Valred)
                    .HasColumnName("VALRED")
                    .HasColumnType("numeric(7, 2)");
            });

            modelBuilder.Entity<MtrTipoOrden>(entity =>
            {
                entity.HasKey(e => e.IdTipoOrden);

                entity.Property(e => e.IdTipoOrden).ValueGeneratedNever();

                entity.Property(e => e.TipoOrden).HasMaxLength(100);
            });

            modelBuilder.Entity<MtrTipoTransaccionCuentaDestino>(entity =>
            {
                entity.HasKey(e => e.IdTransaccionCuentaDestino);

                entity.HasIndex(e => new { e.IdTipoTransaccion, e.IdTipoCuentaDestino })
                    .HasName("IX_MtrTipoTransaccionCuentaDestino")
                    .IsUnique();

                entity.Property(e => e.IdTransaccionCuentaDestino).ValueGeneratedNever();

                entity.Property(e => e.IdTipoTransaccion)
                    .IsRequired()
                    .HasMaxLength(4);
            });

            modelBuilder.Entity<MtrTipoValorRetencion>(entity =>
            {
                entity.HasKey(e => e.IdTipoValor);

                entity.Property(e => e.IdTipoValor).HasMaxLength(1);

                entity.Property(e => e.NombreTipoValor)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MtrUsuarioOficina>(entity =>
            {
                entity.HasKey(e => new { e.Usuario, e.Oficina });

                entity.HasIndex(e => e.Usuario)
                    .HasName("IX_MtrUsuarioOficina");

                entity.Property(e => e.Usuario).HasMaxLength(40);
            });

            modelBuilder.Entity<MtrVendedor>(entity =>
            {
                entity.HasKey(e => e.Codigo);

                entity.HasIndex(e => e.Supervisor)
                    .HasName("IX_MtrVendedor");

                entity.Property(e => e.Codigo)
                    .HasColumnName("CODIGO")
                    .HasMaxLength(4)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.Activo)
                    .IsRequired()
                    .HasColumnName("ACTIVO")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.An8).HasColumnName("AN8");

                entity.Property(e => e.Asignacion)
                    .IsRequired()
                    .HasColumnName("ASIGNACION")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.Categoria)
                    .IsRequired()
                    .HasColumnName("CATEGORIA")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.ClaseVendedor).HasColumnName("CLASE_VENDEDOR");

                entity.Property(e => e.Club300Asistid).HasColumnName("CLUB300_ASISTID");

                entity.Property(e => e.Cobrador)
                    .IsRequired()
                    .HasColumnName("COBRADOR")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.CodDivision).HasColumnName("COD_DIVISION");

                entity.Property(e => e.CodigoGrupo)
                    .IsRequired()
                    .HasColumnName("CODIGO_GRUPO")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.CodigoOverride).HasColumnName("CODIGO_OVERRIDE");

                entity.Property(e => e.CodigoRegion)
                    .IsRequired()
                    .HasColumnName("CODIGO_REGION")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.Comision)
                    .HasColumnName("COMISION")
                    .HasColumnType("decimal(6, 2)");

                entity.Property(e => e.ComisionComprd)
                    .HasColumnName("COMISION_COMPRD")
                    .HasColumnType("decimal(4, 2)");

                entity.Property(e => e.ComisionFactur)
                    .IsRequired()
                    .HasColumnName("COMISION_FACTUR")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.ComisionImpres)
                    .HasColumnName("COMISION_IMPRES")
                    .HasColumnType("decimal(4, 2)");

                entity.Property(e => e.ComisionOrden)
                    .IsRequired()
                    .HasColumnName("COMISION_ORDEN")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.ComisionServ)
                    .HasColumnName("COMISION_SERV")
                    .HasColumnType("decimal(4, 2)");

                entity.Property(e => e.ComisionStock)
                    .HasColumnName("COMISION_STOCK")
                    .HasColumnType("decimal(4, 2)");

                entity.Property(e => e.CompanyBeeper).HasColumnName("COMPANY_BEEPER");

                entity.Property(e => e.Consecutivo).HasColumnName("CONSECUTIVO");

                entity.Property(e => e.CotCorrelativo).HasColumnName("COT_CORRELATIVO");

                entity.Property(e => e.CotizadorPlus).HasMaxLength(1);

                entity.Property(e => e.EMail)
                    .IsRequired()
                    .HasColumnName("E_MAIL")
                    .HasMaxLength(120)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.ExtTelefonica).HasColumnName("EXT_TELEFONICA");

                entity.Property(e => e.FechaIngreso)
                    .HasColumnName("FECHA_INGRESO")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaRetiro)
                    .HasColumnName("FECHA_RETIRO")
                    .HasColumnType("datetime");

                entity.Property(e => e.Ficha)
                    .IsRequired()
                    .HasColumnName("FICHA")
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.FlagAdmin)
                    .HasColumnName("FLAG_ADMIN")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('X')");

                entity.Property(e => e.FlagCalculo)
                    .HasColumnName("FLAG_CALCULO")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('X')");

                entity.Property(e => e.FlagCambio)
                    .IsRequired()
                    .HasColumnName("FLAG_CAMBIO")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.FlagDeGerente)
                    .IsRequired()
                    .HasColumnName("FLAG_DE_GERENTE")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.FlagForaneo)
                    .HasColumnName("FLAG_FORANEO")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FlagGerenteOf)
                    .IsRequired()
                    .HasColumnName("FLAG_GERENTE_OF")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.FlagIc)
                    .HasColumnName("FLAG_IC")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.FlagReplicar)
                    .IsRequired()
                    .HasColumnName("FLAG_REPLICAR")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.FlagRetirado)
                    .IsRequired()
                    .HasColumnName("FLAG_RETIRADO")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.FlagRevisar)
                    .HasColumnName("FLAG_REVISAR")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.FlagSupervisor)
                    .IsRequired()
                    .HasColumnName("FLAG_SUPERVISOR")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.FreeLance)
                    .IsRequired()
                    .HasColumnName("FREE_LANCE")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.Garantia)
                    .IsRequired()
                    .HasColumnName("GARANTIA")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.GerenteOficina)
                    .IsRequired()
                    .HasColumnName("GERENTE_OFICINA")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.GerenteRegion)
                    .IsRequired()
                    .HasColumnName("GERENTE_REGION")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.Grupo).HasColumnName("GRUPO");

                entity.Property(e => e.GrupoVendedoresSap)
                    .HasMaxLength(3)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IdOficinaFisica).HasMaxLength(2);

                entity.Property(e => e.IdOficinaMixProduct)
                    .HasMaxLength(4)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.NombAbreviado)
                    .IsRequired()
                    .HasColumnName("NOMB_ABREVIADO")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.NroDeClientes).HasColumnName("NRO_DE_CLIENTES");

                entity.Property(e => e.NroVendedor).HasColumnName("NRO_VENDEDOR");

                entity.Property(e => e.OfiRefer)
                    .HasColumnName("OFI_REFER")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Oficina).HasColumnName("OFICINA");

                entity.Property(e => e.OrdenRep).HasColumnName("ORDEN_REP");

                entity.Property(e => e.Ordenado).HasColumnName("ORDENADO");

                entity.Property(e => e.Override)
                    .HasColumnName("OVERRIDE")
                    .HasColumnType("decimal(6, 2)");

                entity.Property(e => e.Pais).HasColumnName("PAIS");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("@PASSWORD")
                    .HasMaxLength(9)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.Pedido).HasColumnName("PEDIDO");

                entity.Property(e => e.Posicion).HasColumnName("POSICION");

                entity.Property(e => e.ProduccionInterna)
                    .HasMaxLength(1)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.PuntosDelClub).HasColumnName("PUNTOS_DEL_CLUB");

                entity.Property(e => e.Recnum)
                    .HasColumnName("RECNUM")
                    .HasColumnType("decimal(28, 0)");

                entity.Property(e => e.Supervisor)
                    .IsRequired()
                    .HasColumnName("SUPERVISOR")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.TlfCelular)
                    .IsRequired()
                    .HasColumnName("TLF_CELULAR")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.TlfCelularold)
                    .HasColumnName("TLF_CELULAROld")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.UnidadBeeper)
                    .IsRequired()
                    .HasColumnName("UNIDAD_BEEPER")
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.Usuario)
                    .IsRequired()
                    .HasColumnName("USUARIO")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(' ')");

                entity.HasOne(d => d.OficinaNavigation)
                    .WithMany(p => p.MtrVendedor)
                    .HasForeignKey(d => d.Oficina)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MtrVendedor_MtrOficina");
            });

            modelBuilder.Entity<OfdTipoDocumento>(entity =>
            {
                entity.HasKey(e => e.IdTipoDocumento);

                entity.Property(e => e.IdTipoDocumento).ValueGeneratedNever();

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.IdUsuarioCreacion)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.IdUsuarioModificacion)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.NombreDocumento)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<SegModulo>(entity =>
            {
                entity.HasKey(e => e.IdModulo);

                entity.Property(e => e.IdModulo).ValueGeneratedNever();

                entity.Property(e => e.Descripcion).HasColumnType("text");

                entity.Property(e => e.FlagPortafolioReportes)
                    .IsRequired()
                    .HasMaxLength(1)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Icono)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IconoMenu)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IdTipoReporte).HasDefaultValueSql("((0))");

                entity.Property(e => e.LinkModulo)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NombreModulo)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NombreReporte)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.RutaReporte)
                    .HasMaxLength(500)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ServidorReporte).HasDefaultValueSql("((0))");

                entity.Property(e => e.TipoLlamada)
                    .HasColumnName("TipoLLamada")
                    .HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<SegPrograma>(entity =>
            {
                entity.HasKey(e => e.IdPrograma);

                entity.Property(e => e.IdPrograma).ValueGeneratedNever();

                entity.Property(e => e.Descripcion).HasColumnType("text");

                entity.Property(e => e.FlagRequerimiento).HasMaxLength(1);

                entity.Property(e => e.Link)
                    .HasColumnName("link")
                    .HasMaxLength(200);

                entity.Property(e => e.NombrePrograma)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TipoPrograma)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<SegRol>(entity =>
            {
                entity.HasKey(e => e.IdRol);

                entity.Property(e => e.IdRol).ValueGeneratedNever();

                entity.Property(e => e.AbreviadoRol)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.DescripcionRol).IsRequired();

                entity.Property(e => e.NombreRol)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SegRolEstacion>(entity =>
            {
                entity.HasKey(e => e.IdRolEstacion);

                entity.HasIndex(e => e.IdRol)
                    .HasName("IX_SegRolEstacion_1");

                entity.HasIndex(e => new { e.IdEstacion, e.IdRol })
                    .HasName("IX_SegRolEstacion")
                    .IsUnique();
            });

            modelBuilder.Entity<SegRolModulo>(entity =>
            {
                entity.HasKey(e => e.IdRolModulo);

                entity.HasIndex(e => new { e.IdRol, e.IdModulo })
                    .HasName("IX_SegRolModulo")
                    .IsUnique();

                entity.Property(e => e.IdRolModulo).ValueGeneratedNever();
            });

            modelBuilder.Entity<SegTicket>(entity =>
            {
                entity.HasKey(e => e.TicketSha1);

                entity.Property(e => e.TicketSha1).HasMaxLength(100);

                entity.Property(e => e.FechaHoraCreacion)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FechaHoraGetIp).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraUso).HasColumnType("datetime");

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdUsuario)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.IpUsuario)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<SegUsuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario)
                    .HasName("PK_Usuario");

                entity.HasIndex(e => e.Email)
                    .HasName("IX_Email");

                entity.HasIndex(e => e.Usuario)
                    .HasName("IX_SegUsuario_1")
                    .IsUnique();

                entity.Property(e => e.IdUsuario).ValueGeneratedNever();

                entity.Property(e => e.ClasificacionUsuario)
                    .IsRequired()
                    .HasMaxLength(4)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Clave)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.EquipoImei1)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EquipoImei2)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EquipoImei3)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FechaCambio).HasColumnType("datetime");

                entity.Property(e => e.Ficha)
                    .IsRequired()
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.FlagDisenador)
                    .HasColumnName("Flag_Disenador")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Imei1)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Imei2)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Imei3)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.NombreUsuario)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Usuario)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SegUsuarioRol>(entity =>
            {
                entity.HasKey(e => e.IdUsuarioRol);

                entity.HasIndex(e => new { e.IdUsuario, e.IdRol })
                    .HasName("IX_SegUsuarioRol")
                    .IsUnique();

                entity.Property(e => e.IdUsuarioRol).ValueGeneratedNever();

                entity.HasOne(d => d.IdRolNavigation)
                    .WithMany(p => p.SegUsuarioRol)
                    .HasForeignKey(d => d.IdRol)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SegUsuarioRol_SegRol");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.SegUsuarioRol)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SegUsuarioRol_SegUsuario1");
            });

            modelBuilder.Entity<Sysfile>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.DirVirtualOd)
                    .HasColumnName("DirVirtualOD")
                    .HasMaxLength(50);

                entity.Property(e => e.LinkServer).HasMaxLength(50);

                entity.Property(e => e.LinkServerProduccion).HasMaxLength(50);

                entity.Property(e => e.MailserverName)
                    .HasColumnName("mailserver_name")
                    .HasMaxLength(100);

                entity.Property(e => e.MailserverPort)
                    .HasColumnName("mailserver_Port")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.MailserverType)
                    .HasColumnName("mailserver_type")
                    .HasMaxLength(100);

                entity.Property(e => e.Servidor).HasMaxLength(100);

                entity.Property(e => e.WebServer)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<T006a>(entity =>
            {
                entity.HasKey(e => e.Msehi);

                entity.ToTable("T006A");

                entity.Property(e => e.Msehi)
                    .HasColumnName("MSEHI")
                    .HasMaxLength(18);

                entity.Property(e => e.Mandt)
                    .IsRequired()
                    .HasColumnName("MANDT")
                    .HasMaxLength(18);

                entity.Property(e => e.Mseh3)
                    .IsRequired()
                    .HasColumnName("MSEH3")
                    .HasMaxLength(18);

                entity.Property(e => e.Mseh6)
                    .IsRequired()
                    .HasColumnName("MSEH6")
                    .HasMaxLength(36);

                entity.Property(e => e.Msehl)
                    .IsRequired()
                    .HasColumnName("MSEHL")
                    .HasMaxLength(180);

                entity.Property(e => e.Mseht)
                    .IsRequired()
                    .HasColumnName("MSEHT")
                    .HasMaxLength(60);

                entity.Property(e => e.Spras)
                    .IsRequired()
                    .HasColumnName("SPRAS")
                    .HasMaxLength(6);
            });

            modelBuilder.Entity<TempRbfactura>(entity =>
            {
                entity.HasKey(e => e.Recnum);

                entity.ToTable("TempRBFactura");

                entity.HasIndex(e => new { e.DocRb, e.Factura })
                    .HasName("IX_TempRBFactura")
                    .IsUnique();

                entity.Property(e => e.Recnum)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.DocRb).HasColumnName("DocRB");

                entity.Property(e => e.Fecha)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TempReciboCobro>(entity =>
            {
                entity.HasKey(e => e.Documento);

                entity.HasIndex(e => e.Recnum)
                    .HasName("IX_TempReciboCobro")
                    .IsUnique();

                entity.Property(e => e.Documento).ValueGeneratedNever();

                entity.Property(e => e.FechaRegistro)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Recnum)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
