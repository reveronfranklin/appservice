﻿using AppService.Core;
using AppService.Core.DTOs;
using AppService.Core.Entities;
using AutoMapper;

namespace AppService.Infrastructure.Mappings
{
    public  class AutomapperProfile:Profile
    {
       

        public AutomapperProfile()
        {
            CreateMap<CobGeneralCobranza, CobGeneralCobranzaDto>()
                 .ForMember(dest => dest.NombreCliente, opt => opt.MapFrom(src => src.IdClienteNavigation.Nombre.Trim())); 

            CreateMap<CobGeneralCobranzaDto,CobGeneralCobranza>();

            CreateMap<MtrClienteDto, MtrCliente>();
            CreateMap<MtrCliente, MtrClienteDto>()
                .ForMember(dest => dest.Descripcion,opt=>opt.MapFrom(src=> src.Codigo.Trim() + "-" + src.Nombre.Trim() + " RIF: " +  src.NoRegTribut.ToString()))
                 .ForMember(dest => dest.Codigo, opt => opt.MapFrom(src => src.Codigo.Trim()))
                .ForMember(dest => dest.Nombre, opt => opt.MapFrom(src => src.Nombre.Trim()));

            CreateMap<CobTipoTransaccionDto, CobTipoTransaccion>();
            CreateMap<CobTipoTransaccion, CobTipoTransaccionDto>();
                    
            CreateMap<CobAdjuntosCobranzaDto, CobAdjuntosCobranza>();
            CreateMap<CobAdjuntosCobranza, CobAdjuntosCobranzaDto>();
               
            CreateMap<OfdTipoDocumentoDto, OfdTipoDocumento>();
            CreateMap<OfdTipoDocumento, OfdTipoDocumentoDto>();

            CreateMap<MtrOficinaDto, MtrOficina>();
            CreateMap<MtrOficina, MtrOficinaDto>()
                 .ForMember(dest => dest.NomOficina, opt => opt.MapFrom(src => src.NomOficina.Trim() ));


            CreateMap<MtrVendedorDto, MtrVendedor>()
                 .ForMember(dest => dest.Codigo, opt => opt.MapFrom(src => src.Codigo.Trim()))
             .ForMember(dest => dest.Nombre, opt => opt.MapFrom(src => src.Nombre.Trim()));
            CreateMap<MtrVendedor, MtrVendedorDto>();

            CreateMap<MtrBancosDto, MtrBancos>();
            CreateMap<MtrBancos, MtrBancosDto>();

            CreateMap<MtrTipoMonedaDto, MtrTipoMoneda>();
            CreateMap<MtrTipoMoneda, MtrTipoMonedaDto>();


        }




    }
}
